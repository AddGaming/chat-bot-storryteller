"""
redirects from url shorthands for convenience
"""

from flask import Blueprint, redirect

from web_interface import DOMAIN, PORT, PROTOCOL

redirects = Blueprint("redirect", __name__)


# TODO: Test the redirect
@redirects.route("/", methods=["GET"])
@redirects.route("/home", methods=["GET"])
def home():
    return redirect(f"/app/")


# TODO: Test the redirect
@redirects.route("/register", methods=["GET"])
def register():
    return redirect(f"/app/register")


# TODO: Test the redirect
@redirects.route("/login", methods=["GET"])
def login():
    return redirect(f"/app/login")


# TODO: Test the redirect
@redirects.route("/resources", methods=["GET"])
def resources():
    return redirect(f"/app/resources")


# TODO: Test the redirect
@redirects.route("/impressum", methods=["GET"])
def impressum():
    return redirect(f"/app/impressum")


# TODO: Test the redirect
@redirects.route("/log_view", methods=["GET"])
def log_view():
    return redirect(f"/app/log_view")

# TODO: Test the redirect
@redirects.route("/platform_dashboard", methods=["GET"])
@redirects.route("/platform_overview", methods=["GET"])
def platform_dashboard():
    return redirect(f"/app/platform_dashboard")
