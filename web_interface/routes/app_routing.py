"""
All routes concerning the base application functionality (GUI display) are managed here
"""
import asyncio
import json
import threading

import requests as rq
from flask import Blueprint, request, render_template, flash, redirect, url_for
from flask_login import logout_user, login_required, current_user, login_user

from database import LOGGER, APPDATA, User
from utils.web_functions import lightswitch, nlu_switch
from web_interface import DOMAIN, PROTOCOL, PORT

app_routes = Blueprint("app", __name__)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/", methods=["GET"])
@app_routes.route("/home", methods=["GET"])
def home():
    """
    The homepage of the application.
    This route renders either a dashboard view for logged-in users or a landing page for unknown users.
    """
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
        nlu_switch(request.args.get("set_NLU"))

    if current_user.is_authenticated:
        LOGGER.log(text="Rendered ~/home", platform=LOGGER.Platforms.APPLICATION, category=LOGGER.LogCategory.DEBUG)

        return render_template(
            "coming_soon.html", app=APPDATA, user=current_user
        )

        # call_ana = APPDATA.analyse_calls()

        # Honestly - why does that exist? we can just pass the stats' obj directly. Rewrite prob smarter
        """stats = APPDATA.tracking
        nlu_stats = {"Word_count": stats.word_count,
                     "Intent_count": stats.intent_count,
                     "Entity_count": stats.entity_count,
                     "Uncategorized_word_count": stats["nlu_to_do_entities"],
                     "Categorized_word_count": stats["nlu_word_count"] - stats["nlu_to_do_entities"],
                     "Training_sentence_count": stats["training_count"],
                     "Uncategorized_sentence_count": stats["nlu_to_do_intents"]}

        try:
            percentages = {
                "Discord": call_ana["discord"][3],
                "Twitch": call_ana["twitch"][3],
                "Telegram": call_ana["telegram"][3],
                "Youtube": call_ana["youtube"][3],
                "training_sentence": nlu_stats['Training_sentence_count'] /
                                     ((nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                         'Training_sentence_count']) / 100),
                "uncategorized_sentence_count": nlu_stats['Uncategorized_sentence_count'] /
                                                ((nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                                    'Training_sentence_count']) / 100)
            }
        except ZeroDivisionError:
            percentages = {
                "Discord": call_ana["discord"][3],
                "Twitch": call_ana["twitch"][3],
                "Telegram": call_ana["telegram"][3],
                "Youtube": call_ana["youtube"][3],
                "categorized_word_count": 0, "uncategorized_word_count": 0, "training_sentence": 0,
                "uncategorized_sentence_count": 0
            }"""

        platform_status = asyncio.run(APPDATA.get_platform_status())

        return render_template(
            "home.html", app=APPDATA, user=current_user, tops=APPDATA.top_commands_3(),
            flops=APPDATA.flop_commands_3()
        )
    else:
        LOGGER.log(text="Rendered ~/landing", platform=LOGGER.Platforms.APPLICATION, category=LOGGER.LogCategory.DEBUG)
        return render_template(
            "landing.html", app=APPDATA, user=current_user, doc_url=url_for("app.resources"),
            register_url=url_for("app.register"), chat_url=url_for("app.chat")
        )


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/command-inexport", methods=["GET", "POST"])
@login_required  # make sure this decorator is at the bottom - else doesn't work
def command_inexport():
    pass


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/platform-analysis", methods=["GET", "POST"])
@login_required  # make sure this decorator is at the bottom - else doesn't work
def platform_analysis():
    pass


# TODO: Test this
# TODO: write corresponding html
@app_routes.route("/login", methods=["POST", "GET"])
def login():
    """
    logs in the user in via the api

    Returns: HTML
    """
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        mail = request.form.get("mail")
        pw = request.form.get("pw")
        req_data = {"mail": mail, "pw": pw}
        ans = rq.post(f"{PROTOCOL}://{DOMAIN}:{PORT}/api/login", data=req_data)

        if ans.status_code == 200:
            user = User.query.filter_by(mail=mail).one()
            login_user(user, remember=True)
            return redirect(url_for("app.home"))
        else:
            LOGGER.log(
                text=f"User login from {request.form.get('mail')} failed",
                category=LOGGER.LogCategory.DEBUG,
                platform=LOGGER.Platforms.APPLICATION)
            flash(f"{json.loads(ans.text)['message']}", category="error")

    LOGGER.log(text="Rendered \"/login\"", platform=LOGGER.Platforms.APPLICATION)
    return render_template("login.html", user=current_user, app=APPDATA)


# TODO: Test this
@app_routes.route("/logout")
@login_required
def logout():
    """
    logs out the user in via the api

    Returns: HTML
    """
    LOGGER.log(text=f"User {current_user.mail} logged out", category=LOGGER.LogCategory.INFO)
    logout_user()
    return redirect(url_for("routes.login"))


# TODO: Test this
# TODO: write corresponding html
@app_routes.route("/register", methods=["POST", "GET"])
def register():
    """
    registers the user in via the api

    Returns: HTML
    """
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":

        req_data = {
            "mail": request.form.get("mail"),
            "pw1": request.form.get("pw1"),
            "pw2": request.form.get("pw2"),
            "ott": request.form.get("ott")
        }
        ans = rq.post(f"{PROTOCOL}://{DOMAIN}:{PORT}/api/register", data=req_data)

        if ans.status_code == 200:
            user = User.query.filter_by(mail=request.form.get("mail")).one()
            login_user(user, remember=True)
            return redirect(url_for("app.home"))
        else:
            LOGGER.log(
                text=f"User registration from {request.form.get('mail')} failed",
                category=LOGGER.LogCategory.DEBUG,
                platform=LOGGER.Platforms.APPLICATION)
            flash(f"{json.loads(ans.text)['message']}", category="error")

    return render_template("register.html", user=current_user, app=APPDATA)


# TODO: Test this
# TODO: write corresponding html
@app_routes.route("/profile", methods=["POST", "GET"])
@login_required
def profile():
    """
    a view of all the profile information a user has.

    Returns: HTML
    """
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":

        req_data = {}

        if request.form.get("new-mail"):  # mail update
            req_data["new-mail"] = request.form.get("new-mail")

        if request.form.get("old-pw"):  # pw update
            req_data["old-pw"] = request.form.get("old-pw")
            req_data["new-pw1"] = request.form.get("new-pw1")
            req_data["new-pw2"] = request.form.get("new-pw2")

        ans = rq.post(f"{DOMAIN}/api/profile", data=req_data)

        if ans.status_code != 200:
            LOGGER.log(
                text=f"Received error from API: {ans.text}",
                category=LOGGER.LogCategory.DEBUG,
                platform=LOGGER.Platforms.APPLICATION)
            flash(f"{ans.text}", category="error")

    return render_template("profile.html", user=current_user, app=APPDATA.Application)


# TODO: Test this
# TODO: write corresponding html
@app_routes.route("/otts", methods=["GET", "POST"])
@login_required
def otts():
    """
    A page where users can generate new otts and see how many are left

    Returns: HTML
    """
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    if request.method == "POST":  # to generate new ott
        amount = request.form.get("amount")
        APPDATA.generate_otts(amount)

    ott_count = APPDATA.count_ott()

    return render_template("otts.html", user=current_user, app=APPDATA)


# TODO: Test this
# TODO: write corresponding html
@app_routes.route("/log_view", methods=["GET"])
@login_required
def log_view():

    lightswitch(request.args.get("Light"))

    log_data = APPDATA.list_logs()

    return render_template("log_view.html", logs=log_data, user=current_user, app=APPDATA)


# TODO: Test this
# TODO: write corresponding html
@app_routes.route("/dashboard", methods=["GET"])
@app_routes.route("/command-dashboard", methods=["GET"])
@login_required
def command_dashboard():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    call_ana = APPDATA.analyse_calls()

    return render_template(
        "command_dashboard.html", call_ana=call_ana, user=current_user, flops=APPDATA.analyse_calls(),
        top=APPDATA.top_commands_3(), app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/command-view", methods=["GET", "POST"])
@app_routes.route("/command", methods=["GET", "POST"])
@login_required
def command_view():

    command_id = 1
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
        # TODO: Catch invalid id's
        try:
            command_id = int(request.args.get("id"))
        except:
            command_id = 1

        # return render_template("command_view.html", id=command_id, command=command, ranking=rank,
        #                        groups=groups, author=author, user=current_user, app=APPDATA.Application)

    elif request.method == "POST":
        command_id = request.form.get("current_id")
        if request.form.get("del_flag"):
            from database import Commands
            Commands.query.filter_by(id=command_id).delete()
            APPDATA.session.commit()
            redirect(url_for("routes.command_table"))
        else:
            update_data = [
                request.form.get("command_name"),
                request.form.get("command_symbol"),
                int(request.form.get("group_id")),
                bool(request.form.get("cmd_active")),
                request.form.get("text_discord"),
                request.form.get("text_telegram"),
                request.form.get("text_youtube"),
                request.form.get("text_twitch"),
                bool(request.form.get("cmd_auto_delete")),
                int(request.form.get("cmd_delete_time"))
            ]
            print(update_data)
            APPDATA.update_command(dic=update_data)

    command = Commands.query.filter_by(id=command_id).first()
    from sqlalchemy import desc
    rankings = APPDATA.session.query(Commands).order_by(desc(Commands.usages))
    counter = 0
    rank = -1  # vs compiler warning
    for c in rankings:
        if c.id == command.id:
            rank = counter
            break
        else:
            counter += 1
    from database import Group
    groups = Group.query.all()
    author = User.query.filter_by(id=command.author).first()

    return render_template("command_view.html", id=command_id, command=command, ranking=rank,
                           groups=groups, author=author, user=current_user, app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/platform_dashboard", methods=["GET"])
@app_routes.route("/platform_overview", methods=["GET"])
@login_required
def platform_overview():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    call_ana = APPDATA.analyse_calls()
    platform_status = APPDATA.get_platform_status()

    return render_template(
        "platform_dashboard.html", platform_status=platform_status, calls_today=call_ana, calls_week=call_ana,
        user=current_user, app=APPDATA)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/command-table", methods=["POST", "GET"])
@login_required
def command_table():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    elif request.method == "POST":
        command = [
            request.form.get("cmd_name"),
            request.form.get("cmd_symbol"),
            int(request.form.get("cmd_group")),
            not not request.form.get("cmd_active"),
            request.form.get("cmd_discord"),
            request.form.get("cmd_telegram"),
            request.form.get("cmd_youtube"),
            request.form.get("cmd_twitch"),
            not not request.form.get("cmd_auto_delete"),
            None
        ]
        APPDATA.update_command(dic=command)
    # DEMO DATA START
    from database import Group
    groups = Group.query.all()
    activated_commands = []
    deactivated_commands = []
    from database import Command
    for command in Command.query.all():
        if command.active:
            activated_commands.append(command)
        else:
            deactivated_commands.append(command)

    return render_template("command_table.html", activated_commands=activated_commands,
                           deactivated_commands=deactivated_commands, groups=groups, user=current_user,
                           authors=User.query.all(), app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/command-input", methods=["POST", "GET"])
@app_routes.route("/command-add", methods=["POST", "GET"])
@login_required
def command_input():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        csv = request.form.get("csv")
        APPDATA.parse_csv_to_commands(csv_s=csv)

    return render_template("command_inexport.html", user=current_user, app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/timers")
@login_required
def timer_table():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("coming_soon.html", user=current_user, app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/command-groups")
@login_required
def command_groups():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("coming_soon.html", user=current_user, app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/nlu-dashboard", methods=["GET", "POST"])
@app_routes.route("/nlu-overview", methods=["GET", "POST"])
@login_required
def nlu_overview():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
        nlu_switch(request.args.get("set_NLU"))
    if request.method == "POST":
        if not not request.form.get("key_type"):
            update_data = [request.form.get("key_type"), request.form.get("key_project_id"),
                           request.form.get("key_private_key_id"),
                           request.form.get("key_private_key").replace("\\", "\\\\").replace("\r", ""),
                           request.form.get("key_client_email"), request.form.get("key_client_id"),
                           request.form.get("key_auth_uri"), request.form.get("key_token_uri"),
                           request.form.get("key_auth_provider_x509_cert_url"),
                           request.form.get("key_client_x509_cert_url")]
            all_valid = True
            for e in update_data:
                if e is None:
                    all_valid = False
            if all_valid:
                APPDATA.update_tracking(new_data=update_data)
            else:
                flash("At leased one of the input_ fields is empty", "error")
        elif not not request.form.get("train"):
            flash("The bot starts training. ready in 10 min", "success")

    call_ana = APPDATA.analyse_calls()
    calls = {"Discord_commands": call_ana["discord"][2][0],
             "Twitch_commands": call_ana["twitch"][2][0],
             "Telegram_commands": call_ana["telegram"][2][0],
             "Youtube_commands": call_ana["youtube"][2][0],
             "Discord_nlu": call_ana["discord"][2][1],
             "Twitch_nlu": call_ana["twitch"][2][1],
             "Telegram_nlu": call_ana["telegram"][2][1],
             "Youtube_nlu": call_ana["youtube"][2][1]}

    app = APPDATA.Application
    nlu_stats = {"Word_count": app["nlu_word_count"],
                 "Intent_count": app["intent_count"],
                 "Entity_count": app["entity_count"],
                 "Uncategorized_word_count": app["nlu_to_do_entities"],
                 "Categorized_word_count": app["nlu_word_count"] - app["nlu_to_do_entities"],
                 "Training_sentence_count": app["training_count"],
                 "Uncategorized_sentence_count": app["nlu_to_do_intents"]}

    nlu_calls = call_ana["total_nlu"]
    command_calls = call_ana["total_commands"]

    try:
        # TODO: implement 3Satz and use it here
        percentages = {
            "nlu_calls": nlu_calls * (100 / (command_calls + nlu_calls)),
            "command_calls": command_calls * (100 / (command_calls + nlu_calls)),
            "training_sentence": nlu_stats['Training_sentence_count'] *
                                 (100 / (nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                     'Training_sentence_count'])),
            "categorized_word_count": nlu_stats['Categorized_word_count'] /
                                      ((nlu_stats['Uncategorized_word_count'] + nlu_stats[
                                          'Categorized_word_count']) / 100),
            "uncategorized_word_count": nlu_stats['Uncategorized_word_count'] /
                                        ((nlu_stats['Uncategorized_word_count'] + nlu_stats[
                                            'Categorized_word_count']) / 100),
            "uncategorized_sentence_count": nlu_stats['Uncategorized_sentence_count'] /
                                            ((nlu_stats['Uncategorized_sentence_count'] + nlu_stats[
                                                'Training_sentence_count']) / 100),
            "discord_nlu": calls['Discord_nlu'] * (100 / (calls['Discord_nlu'] + calls['Discord_commands'])),
            "discord_commands": calls['Discord_commands'] * (100 / (calls['Discord_nlu'] + calls['Discord_commands'])),
            "telegram_nlu": calls['Telegram_nlu'] / ((calls['Telegram_nlu'] + calls['Telegram_commands']) / 100),
            "telegram_commands": calls['Telegram_commands'] / (
                    (calls['Telegram_nlu'] + calls['Telegram_commands']) / 100),
            "youtube_nlu": calls['Youtube_nlu'] / ((calls['Youtube_nlu'] + calls['Youtube_commands']) / 100),
            "youtube_commands": calls['Youtube_commands'] / ((calls['Youtube_nlu'] + calls['Youtube_commands']) / 100),
            "twitch_nlu": calls['Twitch_nlu'] / ((calls['Twitch_nlu'] + calls['Twitch_commands']) / 100),
            "twitch_commands": calls['Twitch_commands'] / ((calls['Twitch_nlu'] + calls['Twitch_commands']) / 100)
        }
    except ZeroDivisionError:
        percentages = {
            "nlu_calls": 0, "command_calls": 0, "training_sentence": 0, "categorized_word_count": 0,
            "uncategorized_word_count": 0, "uncategorized_sentence_count": 0, "discord_nlu": 0,
            "discord_commands": 0, "telegram_nlu": 0, "telegram_commands": 0, "youtube_nlu": 0,
            "youtube_commands": 0, "twitch_nlu": 0, "twitch_commands": 0
        }

    from database import Platform
    connection_status = Platform.query.filterby(name="Google").select("status")

    return render_template(
        "nlu_overview.html", nlu_stats=nlu_stats, calls=calls, nlu_calls=nlu_calls, command_calls=command_calls,
        percentages=percentages, call_ana=call_ana, user=current_user, app=APPDATA.Application,
        connection_status=connection_status)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/nlu-entities", methods=["GET", "POST"])
@login_required
def nlu_entities():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    if request.method == "POST":
        word_text = request.form.get("word_text")
        word_status = request.form.get("word_status")
        word_parent = request.form.get("word_parent")  # as id
        word_parent_input = request.form.get("word_parent_input")
        print([word_text, word_status, word_parent, word_parent_input])
        if word_parent_input:
            from database import Entities
            exists = Entities.query.filter_by(name=word_parent_input).first()
            if exists:
                APPDATA.update_nlu_entity([word_text, word_status, exists.id])
                if word_status == "ignore" or word_status == "categorized":
                    APPDATA.Application["nlu_to_do_entities"] -= 1
                else:
                    APPDATA.Application["nlu_to_do_intents"] += 1
                APPDATA.session.commit()
            else:
                new_entity = Entities(name=word_parent_input)
                APPDATA.session.add(new_entity)
                APPDATA.update_nlu_word([word_text, word_status,
                                         Entities.query.filter_by(name=word_parent_input).first().id])
                APPDATA.session.commit()
                if word_status == "ignore" or word_status == "categorized":
                    APPDATA.Application["nlu_to_do_entities"] -= 1
                else:
                    APPDATA.Application["nlu_to_do_intents"] += 1
                APPDATA.session.commit()
        else:
            APPDATA.update_nlu_word([word_text, word_status, word_parent])
            if word_status == "ignore" or word_status == "categorized":
                APPDATA.Application["nlu_to_do_entities"] -= 1
                APPDATA.Application.query.filter_by(id=1).first().to_do_entities -= 1
            else:
                APPDATA.Application["nlu_to_do_intents"] += 1
            APPDATA.session.commit()

    # Page-prep
    entities = Entities.query.all()
    from database import Word
    words = Word.query.all()
    new_words = []
    old_words = []
    for w in words:
        if w.status == "to_do" or w.status == "uncategorized":
            new_words.append(w)
        else:
            old_words.append(w)

    return render_template(
        "nlu_entities.html", new_words=new_words, old_words=old_words, entities=entities, user=current_user,
        app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/nlu-intents", methods=["GET", "POST"])
@login_required
def nlu_intents():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    # TODO: implement post acceptance
    if request.method == "POST":
        text = request.form.get("question_text")
        status = request.form.get("question_status")
        answer_id = request.form.get("answer_id")
        new_answer_text = request.form.get("new_answer")
        print([text, status, answer_id, new_answer_text])
        if new_answer_text:
            exists = APPDATA.filter_outputs(text=new_answer_text)  # <-- TODO: implement
            if exists:
                APPDATA.update_nlu_input([text, status, exists.id])
                if status == "ignore" or status == "categorized":
                    APPDATA.Application["nlu_to_do_intents"] -= 1
                else:
                    APPDATA.Application["nlu_to_do_intents"] += 1
                APPDATA.session.commit()
            else:
                from database import Outputs
                new_answer = Outputs(text=new_answer_text, author=current_user.id)
                APPDATA.session.add(new_answer)
                if status == "ignore" or status == "categorized":
                    APPDATA.Application["nlu_to_do_intents"] -= 1
                else:
                    APPDATA.Application["nlu_to_do_intents"] += 1
                APPDATA.update_nlu_input([text, status, Outputs.query.filter_by(text=new_answer_text).first().id])
                APPDATA.session.commit()
        else:
            if status == "ignore" or status == "categorized":  # TODO: make this ifelse a function
                APPDATA.Application["nlu_to_do_intents"] -= 1
            else:
                APPDATA.Application["nlu_to_do_intents"] += 1
            APPDATA.update_nlu_input([text, status, answer_id])
            APPDATA.session.commit()

    outputs = APPDATA.list_outputs()  # <-- TODO: implement
    inputs = APPDATA.list_inputs()  # <-- TODO: implement
    new_questions = []
    training_phrases = []
    ignored_phrases = []
    for q in inputs:
        if q.status == "to_do" or q.status == "uncategorized":
            new_questions.append(q)
        elif q.status == "categorized":
            training_phrases.append(q)
        else:
            ignored_phrases.append(q)

    return render_template(
        "nlu_intents.html", answers=outputs, new_questions=new_questions, training_phrases=training_phrases,
        ignored_phrases=ignored_phrases, user=current_user, app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/nlu-import", methods=["POST", "GET"])
@login_required
def nlu_import():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))
    if request.method == "POST":
        if request.form.get("csv-questions"):
            APPDATA.parse_csv_to_input(csv_s=request.form.get("csv-questions"))
            LOGGER.log(text="added input_ to the Database", category=LOGGER.LogCategory.INFO)
            flash("Questions added to Database", "success")
        elif request.form.get("csv-answers"):
            APPDATA.parse_csv_to_output(csv_s=request.form.get("csv-answers"))
            LOGGER.log(text="added output to the Database", category=LOGGER.LogCategory.INFO)
            flash("Answers added to Database", "success")
        elif request.form.get("csv-entities"):
            APPDATA.parse_csv_to_entity(csv_s=request.form.get("csv-entities"))
            LOGGER.log(text="added entities to the Database", category=LOGGER.LogCategory.INFO)
            flash("Entities added to Database", "success")
        elif request.form.get("csv-words"):
            APPDATA.parse_csv_to_words(csv_s=request.form.get("csv-words"))
            LOGGER.log(text="added words to the Database", category=LOGGER.LogCategory.INFO)
            flash("Words added to Database", "success")
        else:
            flash("You submitted an empty form", category="error")

    return render_template("nlu_inexport.html", user=current_user, app=APPDATA.Application)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/impressum")
def impressum():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("impressum.html", user=current_user, app=APPDATA)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/resources")
def resources():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("resources.html", user=current_user, app=APPDATA)


# TODO: Implement this
# TODO: write corresponding html
@app_routes.route("/chat")
@app_routes.route("/chat-test")
def chat():
    if request.method == "GET":
        lightswitch(request.args.get("Light"))

    return render_template("chat.html", user=current_user, app=APPDATA.Tracking)
