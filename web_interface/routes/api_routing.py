"""
All routes concerning the extended functionality are managed here
"""
import asyncio

from flask import Blueprint, request, jsonify, session
from flask_login import login_user, logout_user, login_required, current_user
from database import LOGGER, APPDATA, User, UserSettingProfile
from utils.crypto import hash_pw_async, check_pw, hash_pw
from web_interface import _USER_DB
import database.data_tools.data_validation as validation
import utils.errors

api_routes = Blueprint("api_routes", __name__)


# TODO: Test this
@api_routes.route("/login", methods=["POST"])
def login():
    """
    The function handles login via the api. If bad or missing data is submitted 400 are thrown.
    If login succeeds 200 is returned.

    Returns: message in json + HTTP Status + session cookie
    """
    response = {"message": ""}
    try:
        mail = request.values["mail"]
        pw = request.values["pw"]
        validation.validate_login_data(mail, pw)
    except KeyError as e:
        response["message"] = f"Missing argument. Either mail and or password were not submitted"
        return jsonify(response), 400
    except utils.errors.IllegalArgument as e:
        response["message"] = f"Email or Password contain Illegal Letters"
        return jsonify(response), 400

    user = User.query.filter_by(mail=mail).first()
    if user:
        if asyncio.run(check_pw(pw, user.pw)):
            login_user(user, remember=True)
            LOGGER.log(
                text=f"Logged in User {current_user.mail}",
                category=LOGGER.LogCategory.DEBUG,
                platform=LOGGER.Platforms.BACKEND)
            response["message"] = f"Login Successful"
            session.permanent = True
            session["mail"] = mail
            session["id"] = current_user.id
            session["admin"] = current_user.admin
            return jsonify(response), 200
        else:
            LOGGER.log(
                text=f"Login failed from {mail} due to invalid password",
                category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
            response["message"] = f"Login failed: The password or the email is wrong"
            return jsonify(response), 400
    else:
        LOGGER.log(
            text=f"Login failed from {mail} since the mail is not registered",
            category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
        response["message"] = f"The given mail is not registered in this application"
        return jsonify(response), 400


# TODO: Test this
@api_routes.route("/logout", methods=["POST"])
@login_required
def logout():
    """
    Logs out the user. Requires a logged-in user.

    Returns: message in json + HTTP Status
    """
    LOGGER.log(
        text=f"User {current_user.mail} logged out",
        category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
    logout_user()
    response = {"message": f"Logout successful"}
    return jsonify(response), 200


# TODO: Test this
@api_routes.route("/register", methods=["POST"])
def register():
    """
    Register a new user in the application

    Returns: message in json + HTTP Status + session cookie
    """
    response = {"message": ""}
    try:
        mail = request.values["mail"]
        pw1 = request.values["pw1"]
        pw2 = request.values["pw2"]
        validation.validate_login_data(mail, pw1)
        validation.validate_login_data(mail, pw2)
    except KeyError as e:
        response["message"] = f"Missing argument. Expected: mail, password1, password2"
        return jsonify(response), 400
    except utils.errors.IllegalArgument as e:
        response["message"] = f"Email or Password contain Illegal Letters"
        return jsonify(response), 400

    try:
        ott = request.values["ott"]
    except KeyError as e:
        ott = None

    admin = False
    unique = not User.query.filter_by(mail=mail).first()
    if not unique or len(mail) < 4:
        LOGGER.log(
            f"Failed register due malformed email: {mail}",
            category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
        response["message"] = f"{mail=} | is invalid. Make sure it is longer than 4 characters and not already in use"
        return jsonify(response), 400
    elif pw1 != pw2:
        LOGGER.log(
            "Failed register due not matching passwords",
            category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
        response["message"] = f"{pw1=}, {pw2=} | passwords are not matching"
        return jsonify(response), 400
    elif len(pw1) < 8:
        LOGGER.log(
            "Failed register since password is to short",
            category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
        response["message"] = f"{pw1=} | is too short"
        return jsonify(response), 400

    if ott:
        if APPDATA.check_ott(ott):
            admin = True
        else:
            LOGGER.log(
                "Failed register due to Invalid ott",
                category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
            response["message"] = f"{ott=} | is invalid"
            return jsonify(response), 400

    LOGGER.log("User checks successful", platform=LOGGER.Platforms.BACKEND, category=LOGGER.LogCategory.DEBUG)

    pw_hash = hash_pw(pw1)
    new_user = User(mail=mail, pw=pw_hash, admin=admin)
    new_user_settings = UserSettingProfile(user_id=new_user.id)

    LOGGER.log("User created", platform=LOGGER.Platforms.BACKEND, category=LOGGER.LogCategory.DEBUG)

    _USER_DB.session.add(new_user)
    _USER_DB.session.add(new_user_settings)
    _USER_DB.session.commit()

    login_user(new_user, remember=True)
    session.permanent = True
    session["mail"] = mail
    session["id"] = current_user.id
    session["admin"] = current_user.admin
    response["message"] = f"{mail=} registered and logged in successfully"
    return jsonify(response), 200


# TODO: test this
@api_routes.route("/otts", methods=["GET", "POST"])
def otts():
    """
    Shows how many otts are currently available in return message.

    Returns: json + HTTP Status
    """
    if session.get("admin", False):
        ott_count = APPDATA.count_ott()
        response = {"message": ott_count}
        return jsonify(response), 200
    else:
        response = {"message": "your permission lv is to low for this action.\nYou need to be admin to do this!"}
        return jsonify(response), 403



# TODO: test this
@api_routes.route("/log_view", methods=["GET", "POST"])
@login_required
def log_view():
    """
    The logs until the last error + 100 logs before in the http message
    eg: newest ... newest error ... [newest error+100]

    Returns: logs in json + HTTP Status
    """

    log_data = APPDATA.list_logs()
    response = {"message": log_data}
    return jsonify(response), 200


# TODO: Optimize this
# TODO: test this
@api_routes.route("/profile", methods=["POST", "GET"])
@login_required
def profile():
    """
    Returns: user information in json + HTTP Status
    """
    if request.method == "GET":  # get information
        dic = APPDATA.user_by_mail(current_user.mail)  # <-- TODO: implement
        response = {"message": dic}
        return jsonify(response), 200

    elif request.method == "POST":  # post information you want to change
        if request.args["new-mail"]:  # mail update
            new_mail = request.args["new-mail"]
            if len(new_mail) > 4:
                User.query.filter_by(id=current_user.id).first().mail = new_mail
                _USER_DB.session.commit()
            else:
                LOGGER.log(
                    text=f"User {current_user.id} failed to change emails",
                    category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
                response = {"message": f"The new mail address is too short: {new_mail}"}
                return jsonify(response), 400

        elif request.args["old-pw"]:  # password update
            old_pw = request.args["old-pw"]
            new_pw1 = request.args["new-pw1"]
            new_pw2 = request.args["new-pw2"]
            if new_pw2 != new_pw1:
                LOGGER.log(
                    text=f"User {current_user.id} failed to change Passwords since they didn't match",
                    category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
                response = {"message": f"Password 1 and Password 2 do not match:\n{new_pw1=}\n{new_pw2=}"}
                return jsonify(response), 400
            elif not check_pw(old_pw, current_user.pw):
                LOGGER.log(
                    text=f"User {current_user.id} failed to change the password-confirmation to change his password",
                    category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
                response = {"message": f"Your current Password and the Password you entered do not match"}
                return jsonify(response), 400
            else:
                User.query.filter_by(id=current_user.id).first().pw = hash_pw_async(new_pw1)
                LOGGER.log(
                    text=f"User {current_user.id} changed pw",
                    category=LOGGER.LogCategory.INFO, platform=LOGGER.Platforms.BACKEND)
                response = {"message": "successfully change password"}
                return jsonify(response), 200


# TODO: implement this
# TODO: test this
@api_routes.route("/command", methods=["GET", "POST"])
@api_routes.route("/command-view", methods=["GET", "POST"])
def command_view():
    """
    GET: get information about the command given as id in parameters

    POST: change information about that command given command-id and information to change

    Returns: json message + HTTP Status
    """
    if request.method == "GET":
        command_id = int(request.args.get("id"))  # TODO: what if faster? Dictionary access or .get()?
        command = APPDATA.filter_commands(id_=command_id)[0]
        return "hi"

    elif request.method == "POST":
        return "hi"


# necessary for testing.
# TODO: test that you cant reach this without admin right!!!
@api_routes.route("/kys", methods=["GET"])
def kys():
    """
    shuts down the server

    Returns: Shut down message
    """
    if session.get("admin", False):
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()
        return "I'm dead"
    else:
        response = {"message": "your permission lv is to low for this action.\nYou need to be admin to do this!"}
        return jsonify(response), 403
