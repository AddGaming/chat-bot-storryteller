from concurrent.futures import ThreadPoolExecutor
from queue import Queue
from threading import Thread


class ThreadQueue:

    def __init__(self, pool_size=5):
        self.work = Queue()
        self.pool = ThreadPoolExecutor(max_workers=pool_size)

    def _run_single(self):
        while True:
            if not self.work.empty():
                dic = self.work.get()
                dic["func"](*dic["args"], **dic["kwargs"])

    def _run_multi(self):
        while True:
            if not self.work.empty():
                dic = self.work.get()
                self.pool.submit(dic["func"], *dic["args"], **dic["kwargs"])

    def run(self):
        Thread(target=self._run_multi, daemon=True).start()

    def push(self, func, *args, **kwargs):
        dic = {"func": func, "args": args, "kwargs": kwargs}
        self.work.put(dic)
