from flask_login import current_user

from database import APPDATA, LOGGER


def lightswitch(mode):
    if mode is not None:
        # TODO: rework. this should not work!
        current_user.light_mode = bool(int(mode))
        # TODO: Catch invalid input_


def nlu_switch(mode):
    if mode is not None:
        match mode:
            case "0":
                APPDATA.tracking.nlu = bool(int(mode))
                LOGGER.log(text=f"NLU-Status set to {bool(int(mode))}", category=LOGGER.LogCategory.DEBUG)
            case "1":
                APPDATA.tracking.nlu = bool(int(mode))
                LOGGER.log(text=f"NLU-Status set to {bool(int(mode))}", category=LOGGER.LogCategory.DEBUG)
            case _:
                LOGGER.log(
                    text=f"Invalid Parameter: NLU-Status could not be set to {mode}",
                    category=LOGGER.LogCategory.WARNING)
