"""
Since some things change from os to os, they get dynamically assigned/ used form here
"""
from utils.errors import UnsupportedOS


def __copy_dir(from_path: str, to_path: str, symlinks=False, ignore=None) -> None:
    """
    copies directories.

    Args:
        from_path: str - directory to copy
        to_path: str - destination
        symlinks:
        ignore:

    Returns: None
    """
    import os
    import shutil
    dir_items = os.listdir(from_path)
    for item in dir_items:
        s = os.path.join(from_path, item)
        d = os.path.join(to_path, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def create_test_env(paths=None):
    """
    For loading dummy data into a test. Use it "below" @create_test_env
    The paths are relative. it is expected that they are contained in tests.dummy_data

    Args:
        paths: list of tuples - e.g: [(dummy_source_path, dummy_target_path),...]

    Returns: the decorated function
    """

    def decorator(fn):
        """
        Creates and deletes environments for tests when decorating a function with this.
        If the test fails, the environment doesn't get deleted.

        Returns: decorated function
        """

        def wrapper(*args, **kwargs):
            from utils.crypto import time_hash
            import os
            import shutil

            # create env
            bad_chars = "=\\+.,?/"
            dir_name = time_hash()
            for char in bad_chars:
                dir_name = dir_name.replace(char, "")

            from_path = __file__.split(f"{os.sep}")
            while from_path[-1] != "chat-bot-storyteller":
                from_path.pop()
            from_path = os.sep.join(from_path)

            if not os.path.isdir(f"{from_path}{os.sep}tests{os.sep}test_cash"):
                os.mkdir(f"{from_path}{os.sep}tests{os.sep}test_cash")

            to_path = f"{from_path}{os.sep}tests{os.sep}test_cash{os.sep}{dir_name}"
            old_chdir = os.getcwd()

            os.mkdir(to_path)

            # costume copy dir function for controll over what to copy
            dir_items = os.listdir(from_path)
            dont_copy = [".idea", "tests", ".git", "devOpsTools", "venv", "__pycache__"]
            for e in dont_copy:
                if e in dir_items:
                    dir_items.remove(e)

            for item in dir_items:
                s = os.path.join(from_path, item)
                d = os.path.join(to_path, item)
                if os.path.isdir(s):
                    shutil.copytree(s, d, symlinks=False, ignore=None)
                else:
                    shutil.copy2(s, d)
            os.chdir(to_path)

            # copy dummies

            if paths:
                for path in paths:
                    # print(f"COPY\n{from_path}{os.sep}tests{os.sep}dummy_data{os.sep}{path[0]}->\n"
                    #       f"{to_path}{os.sep}{path[1]}")
                    shutil.copy2(
                        f"{from_path}{os.sep}tests{os.sep}dummy_data{os.sep}{path[0]}",
                        f"{to_path}{os.sep}{path[1]}")

            # exec test
            try:
                fn(*args, **kwargs)
            finally:
                # clean up afterwards
                os.chdir(old_chdir)
                shutil.rmtree(to_path)

        return wrapper

    return decorator


def load_package(packages=None):
    """
    reloads all packages listed. If packages is not loaded yet, it will get loaded.

    Args:
        packages: [str] - list of package names

    Returns: decorated function
    """

    def decorator(fn):
        def wrapper(*args, **kwargs):
            import importlib.util
            import sys
            for package_name in packages:
                spec = importlib.util.find_spec(package_name)
                module = importlib.util.module_from_spec(spec)
                sys.modules[package_name] = module
                spec.loader.exec_module(module)

            fn(*args, **kwargs)

        return wrapper

    return decorator


def boot_flask(fn):
    def wrapper(*args, **kwargs):
        import subprocess
        import platform
        import requests
        import os
        from datetime import datetime
        from flask import request

        if platform.system() == "Linux":
            p = subprocess.Popen(["python", "main.py"])
        elif platform.system() == "Windows":
            p = subprocess.Popen(["py", "main.py"])
        elif platform.system() == "Darwin":
            p = subprocess.Popen(["python", "main.py"])
        else:
            raise UnsupportedOS("The OS is not known. Supported are Windows, Linux, Mac")

        print(f"Loading flask from {os.getcwd()}")

        # boot time
        start = datetime.now()
        while True:
            try:
                code = requests.get("http://127.0.0.1:5000/").status_code
            except requests.exceptions.ConnectionError:
                print("...booting")
                continue

            if code == 200:
                break
            else:
                print(code)
                break
        end = datetime.now()

        print(f"booting duration: {end-start}")

        fn(*args, **kwargs)

        login_data = {
            "mail": "test_mail",
            "pw": "password",
        }
        a = requests.post("http://127.0.0.1:5000/api/login", data=login_data)
        requests.get("http://127.0.0.1:5000/api/kys", cookies=a.cookies)
        p.kill()
        p.wait()  # wait for the process to finish shutting down

    return wrapper
