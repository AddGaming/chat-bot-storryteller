"""
The starting point of the application. To start the Web-Server execute this file
"""

from web_interface import create_app


def main():
    """
    The starting point of the application. To start the Web-Server execute this file
    """
    app = create_app()
    app.run(debug=True)


if __name__ == "__main__":
    main()
