import asyncio
import unittest

from utils.db_logger import DBLogger
from utils.errors import InvalidLogLV, InvalidLogPlatform


class TestLogger(unittest.TestCase):

    def test_default_call(self):
        from database import APPDATA, LOGGER
        x = len(asyncio.run(APPDATA.list_logs()))
        LOGGER.log("test")
        llist = asyncio.run(APPDATA.list_logs())
        y = len(llist)
        self.assertEqual(y-x, 1)
        e = llist[y-1]
        self.assertEqual(e[1], "test")
        self.assertEqual(e[3], "BackEnd")
        self.assertEqual(e[4], 0)

    def test_parameterized_call(self):
        from database import APPDATA, LOGGER
        x = len(asyncio.run(APPDATA.list_logs()))
        LOGGER.log(
            "my fancy info",
            category=LOGGER.LogCategory.INFO,
            platform=LOGGER.Platforms.TELEGRAM)
        llist = asyncio.run(APPDATA.list_logs())
        y = len(llist)
        self.assertEqual(y-x, 1)
        e = llist[y - 1]
        self.assertEqual(e[1], "my fancy info")
        self.assertEqual(e[3], LOGGER.Platforms.TELEGRAM.value)
        self.assertEqual(e[4], LOGGER.LogCategory.INFO.value)

    def test_log_call_with_lower_priority(self):
        from database import APPDATA, LOGGER
        mylogger = DBLogger(log_lv=DBLogger.LogCategory.HIGH)
        mylogger.log("not empty", category=mylogger.LogCategory.ERROR)
        x = len(asyncio.run(APPDATA.list_logs()))
        mylogger.log("test", category=LOGGER.LogCategory.DEBUG)
        y = len(asyncio.run(APPDATA.list_logs()))
        self.assertEqual(y-x, 0)

    def test_log_call_with_higher_priority(self):
        from database import APPDATA, LOGGER
        x = len(asyncio.run(APPDATA.list_logs()))
        LOGGER.log("test", category=LOGGER.LogCategory.ERROR)
        y = len(asyncio.run(APPDATA.list_logs()))
        self.assertEqual(y-x, 1)

    def test_try_log_lv_oob_lower(self):
        with self.assertRaises(InvalidLogLV):
            DBLogger().try_log_lv(-1)

    def test_try_log_lv_oob_upper(self):
        with self.assertRaises(InvalidLogLV):
            DBLogger().try_log_lv(4)

    def test_try_log_lv_valid_int(self):
        ret = DBLogger().try_log_lv(2)
        self.assertEqual(ret, 2)

    def test_try_log_lv_valid_enum(self):
        l = DBLogger()
        ret = l.try_log_lv(l.LogCategory.WARNING)
        self.assertEqual(ret, 2)

    def test_try_log_lv_invalid_type(self):
        with self.assertRaises(InvalidLogLV):
            DBLogger().try_log_lv("string")

    def test_try_platform_valid_str(self):
        ret = DBLogger().try_platform("discord")
        self.assertEqual(ret, "Discord")

    def test_try_platform_valid_enum(self):
        l = DBLogger()
        ret = l.try_platform(l.Platforms.API)
        self.assertEqual(ret, "Api")

    def test_try_platform_invalid_type(self):
        with self.assertRaises(InvalidLogPlatform):
            DBLogger().try_platform(3)

    def test_try_platform_invalid_str(self):
        with self.assertRaises(InvalidLogPlatform):
            DBLogger().try_platform("Imaginary")


if __name__ == '__main__':
    unittest.main()
