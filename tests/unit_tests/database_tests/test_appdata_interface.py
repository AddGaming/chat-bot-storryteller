import unittest
import os

from utils.decorators import create_test_env

# TODO: implement this
"""
class TestInit(unittest.TestCase):
    
    def test_valid_args(self):
        self.assertEqual(True, False)  # TODO

    def test_invalid_args(self):
        self.assertEqual(True, False)  # TODO
        
    def test_no_args(self):
        self.assertEqual(True, False)  # TODO
"""


class TestSavingTracking(unittest.TestCase):

    @create_test_env()
    def test_save_new_file(self):
        from database import AppDataInterface

        try:
            os.remove(f"{os.getcwd()}{os.sep}database{os.sep}tracking.json")
        except FileNotFoundError:
            pass

        tk = AppDataInterface._Tracking()
        tk.save()

        d = os.listdir(f"{os.getcwd()}{os.sep}database")
        self.assertTrue('tracking.json' in d)

    @create_test_env()
    def test_overwrite_file(self):
        from database import AppDataInterface

        # TODO: create file. Low prio since current implementation will not be affected by this

        tk = AppDataInterface._Tracking()
        tk.save()

        d = os.listdir(f"{os.getcwd()}{os.sep}database")
        self.assertTrue('tracking.json' in d)

        os.remove(f"{os.getcwd()}{os.sep}database{os.sep}tracking.json")


class TestLoadingTracking(unittest.TestCase):

    @create_test_env(paths=[("tracking_valid.json", f"database{os.sep}tracking.json")])
    def test_loading_valid(self):
        from database import AppDataInterface

        tk = AppDataInterface._Tracking.load()

        self.assertEqual(tk.word_count, 69,
                         f"The file didnt get loaded. Expected 69, got {tk.word_count}")

    @create_test_env()
    def test_loading_missing(self):
        from database import AppDataInterface

        AppDataInterface._Tracking.load()

    @create_test_env(paths=[("invalid_json_tracking.json", f"database{os.sep}tracking.json")])
    def test_loading_invalid_json_format(self):
        from database import AppDataInterface

        AppDataInterface._Tracking.load()

    @create_test_env(paths=[("corrupt_tracking.json", f"database{os.sep}tracking.json")])
    def test_loading_invalid_data(self):
        from database import AppDataInterface

        tk = AppDataInterface._Tracking.load()

        self.assertNotEqual("yes", tk.to_do_intents, "you parsed a corrupted json!")


if __name__ == '__main__':
    unittest.main()
