"""
Tests concerning data transformation
"""
import unittest


class TestCascadeCommandText(unittest.TestCase):

    def test_default(self):
        self.assertTrue(True)

    # TODO: activate and fix
"""
    
    def test_all_empty(self):
        from database.data_tools.data_transformation import TextEffectSyntax

        discord, telegram, youtube, twitch = "", "", "", ""
        new = TextEffectSyntax.cascade(discord, telegram, youtube, twitch)
        self.assertEqual(new, ["", "", "", ""])

    def test_discord_only(self):
        # TODO: Adjust function calls to new function
        discord, telegram, youtube, twitch = "**Discord** is *awesome*, that is ~~sometimes~~ __always__!", "", "", ""
        new = cascade_command_text(discord, telegram, youtube, twitch)
        self.assertEqual(
            new,
            ["**Discord** is *awesome*, that is ~~sometimes~~ __always__!",
             "**Discord** is *awesome*, that is ~~sometimes~~ always!",
             "Discord is awesome, that is sometimes always!",
             "Discord is awesome, that is sometimes always!"]
        )

    def test_telegram_only(self):
        from database.database import cascade_command_text
        discord, telegram, youtube, twitch = "", "**Telegram** is *awesome*, that is ~~sometimes~~ always!", "", ""
        new = cascade_command_text(discord, telegram, youtube, twitch)
        self.assertEqual(
            new,
            ["**Telegram** is *awesome*, that is ~~sometimes~~ always!",
             "**Telegram** is *awesome*, that is ~~sometimes~~ always!",
             "Telegram is awesome, that is sometimes always!",
             "Telegram is awesome, that is sometimes always!"]
        )

    def test_youtube_and_telegram(self):
        from database.database import cascade_command_text
        discord, twitch = "", ""
        telegram = "**Telegram** is *awesome*, that is ~~sometimes~~ always!"
        youtube = "Youtube text!"
        new = cascade_command_text(discord, telegram, youtube, twitch)
        self.assertEqual(
            new,
            ["**Telegram** is *awesome*, that is ~~sometimes~~ always!",
             "**Telegram** is *awesome*, that is ~~sometimes~~ always!",
             "Youtube text!",
             "Youtube text!"]
        )

    def test_discord_to_html(self):
        # TODO: implement this
        pass

    def test_html_to_discord(self):
        # TODO: implement this
        pass
"""

if __name__ == '__main__':
    unittest.main()
