import asyncio
import unittest


class TestHashPw(unittest.TestCase):

    def test_2_hashes_of_the_same_are_salted(self):
        from utils.crypto import hash_pw_async
        self.assertNotEqual(asyncio.run(hash_pw_async("hallo")), asyncio.run(hash_pw_async("hallo")))

    def test_2_hash_is_not_clear_text(self):
        from utils.crypto import hash_pw_async
        self.assertNotEqual(asyncio.run(hash_pw_async("hallo")), "hallo")

    def test_time_needed_to_encrypt(self):
        from utils.crypto import hash_pw_async
        import datetime

        start = datetime.datetime.now()
        asyncio.run(hash_pw_async("a"))
        end = datetime.datetime.now()

        self.assertTrue((end-start).seconds >= 1, f"The encryption time is to low!\n{end-start}")


class TestCheckPw(unittest.TestCase):

    def test_checking_successful(self):
        from utils.crypto import hash_pw_async, check_pw
        h1 = asyncio.run(hash_pw_async("hallo"))
        self.assertTrue(asyncio.run(check_pw("hallo", h1)))

    def test_checking_failing(self):
        from utils.crypto import hash_pw_async, check_pw
        h1 = asyncio.run(hash_pw_async("hallo"))
        self.assertFalse(asyncio.run(check_pw("This is false", h1)))


class TestTimeHashPw(unittest.TestCase):

    def test_2_different_times_should_have_different_hashes(self):
        from utils.crypto import time_hash
        h1 = time_hash()
        h2 = time_hash()
        self.assertNotEqual(h1, h2)

    def test_same_time_has_same_hash(self):
        from utils.crypto import time_hash
        import datetime
        dtime = datetime.datetime.now()
        h1 = time_hash(t=dtime)
        h2 = time_hash(t=dtime)
        self.assertEqual(h1, h2)


if __name__ == '__main__':
    unittest.main()
