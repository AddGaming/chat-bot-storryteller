"""
Test the functions of teh the Dialogflow-Interface
"""
import unittest
from utils.errors import *

import google.cloud.dialogflow_v2 as dialogflow_vx

import apis.wrapper.dialogflow as df


class TestDialogflowInterface(unittest.TestCase):

    def test_entity_type_missing_parameters(self):
        self.assertRaises(MissingArgument, df.create_entity_type)
        self.assertRaises(AttributeError, df.create_entity_type, 2, 7)

    def test_intent_missing_parameters(self):
        self.assertRaises(MissingArgument, df.create_intent)
        self.assertRaises(ParameterRequired, df.create_intent, 1, 2, 3, 4)
        self.assertRaises(AttributeError, df.create_intent, 1, 2, 3)

    def test_annotate_missing_parameters(self):
        self.assertRaises(MissingArgument, df.build_phrases)
        self.assertRaises(TypeError, df.build_phrases, 2, 7)

    def test_annotate_no_entity_required(self):
        # test fo a single string
        testphrase = "I really hope this works"
        part1 = dialogflow_vx.Intent.TrainingPhrase.Part()
        part1.text = testphrase
        dialogflow_written = dialogflow_vx.Intent.TrainingPhrase(parts=[part1])
        self.assertEqual([dialogflow_written], df.build_phrases([testphrase]))
        # test for multiple phrases
        testphrase2 = "I still hope it works"
        part2 = dialogflow_vx.Intent.TrainingPhrase.Part()
        part2.text = testphrase2
        dialogflow_written2 = dialogflow_vx.Intent.TrainingPhrase(parts=[part2])
        strlist = [testphrase, testphrase2]
        diallist = [dialogflow_written, dialogflow_written2]
        self.assertEqual(diallist, df.build_phrases(strlist))

    def test_annotate_with_entity_required(self):
        testphrase = "I like Trains"
        part1 = dialogflow_vx.Intent.TrainingPhrase.Part(text="I")
        part2 = dialogflow_vx.Intent.TrainingPhrase.Part(text="like")
        part3 = dialogflow_vx.Intent.TrainingPhrase.Part(text="Trains")
        part3.alias = "Vehicle"
        part3.user_defined = True
        part3.entity_type = "@" + "Vehicle"
        part_white = dialogflow_vx.Intent.TrainingPhrase.Part(text=" ")
        parts = [part1, part_white, part2, part_white, part3, part_white]
        dialogflow_written = dialogflow_vx.Intent.TrainingPhrase(parts=parts)
        entity_type = dialogflow_vx.EntityType()
        entity_type.kind = "KIND_LIST"
        entity_type.display_name = "Vehicle"
        entity = dialogflow_vx.EntityType.Entity()
        entity.value = "Trains"
        entity.synonyms = "Trains"
        entity_type.entities = [entity]
        function_result = df.build_phrases([testphrase], entity_type)
        self.assertEqual([dialogflow_written], function_result)
        self.assertEqual(dialogflow_written, function_result[0])
        self.assertEqual(dialogflow_written.parts, function_result[0].parts)
        self.assertEqual(dialogflow_written.parts[0], function_result[0].parts[0])
        print(function_result)

    def test_building_entity(self):
        typename = "Vehicle"
        entries = ["Car", "Trains", "Bus", "Motorcycle", "Car"]
        result = df.create_entity_type(typename, entries)
        print(result)
        print("HEEEEEEEEEEEREEEEEEEEEEEE ITTTTTTTTT COMMMMMMMMMMMMMMES")
        print(result.name)
        self.assertEqual(typename, result.display_name)
        for entity in result.entities:
            self.assertIn(entity.value, entries)
            print(entity.value)

    def test_building_entity_illegal_characters(self):
        typename = "Vehiäö/cle"
        sanitized_typename = "Vehicle"
        entries = ["Car", "Trains", "Bus", "Motorcycle", "Car"]
        result = df.create_entity_type(typename, entries)
        print(result)
        self.assertEqual(sanitized_typename, result.display_name)
        print(result.entities)

    def test_create_intent_no_context_no_required_entities(self):
        trainingphrases = ["I like Trains", "I Love my Car", "I adore Bus"]
        intent_name = "Vehicle_Addict"
        response = "get some help"
        result = df.create_intent(trainingphrases, response, intent_name)
        print(result)
        self.assertEqual(result.display_name, intent_name)
        self.assertEqual(result.messages[0].text.text, [response])

    def test_create_intent_no_context_no_required_entities_illegal_name(self):
        trainingphrases = ["I like Trains", "I Love my Car", "I adore Bus"]
        intent_name = "Vehäöicle Add++ict"
        sanitized_name = "Vehicle Addict"
        response = "get some help"
        result = df.create_intent(trainingphrases, response, intent_name)
        print(result)
        self.assertEqual(result.display_name, sanitized_name)
        self.assertEqual(result.messages[0].text.text, [response])

    def test_create_intent_with_required_entities(self):
        trainingphrases = ["I like Trains", "I Love my Car", "I adore Bus"]
        intent_name = "Vehicle Addict"
        response = "get some help"
        required_entity_type_name = "Vehicle"
        required_entity_type_entries = ["Car", "Trains", "Bus", "Motorcycle", "Car"]
        required_entity_type = df.create_entity_type(
            required_entity_type_name, required_entity_type_entries
        )
        print(required_entity_type)
        prompts_for_required = ["What is the Vehicle you admire"]
        result = df.create_intent(
            trainingphrases, response, intent_name, required_entity_type,
            prompts_for_required
        )
        print(result)
        self.assertEqual(result.display_name, intent_name)
        self.assertEqual(result.messages[0].text.text, [response])
        self.assertEqual(result.parameters[0].prompts, prompts_for_required)
