import unittest

from utils.decorators import create_test_env, load_package


class TestDataPackageInit(unittest.TestCase):

    @create_test_env()
    @load_package(["database", "web_interface"])
    def test_boot_from_empty_environment(self):
        import os

        from database import APPDATA
        from web_interface import create_app

        create_app()
        d = os.listdir(f"{os.getcwd()}{os.sep}database")
        try:
            d.index('appdata.db')
            d.index('userdata.db')
            d.index('worlddata.db')
        except ValueError:
            self.assertTrue(False,
                            f"The database files were not properly created!\nDir: {d}\nCwd: {os.getcwd()}")
        APPDATA.db_session.close()

    @create_test_env()
    @load_package(["database"])
    def test_boot_from_normal_environment(self):
        from database import APPDATA

        APPDATA.db_session.close()
        # TODO: Add dummy data to load from!


if __name__ == '__main__':
    unittest.main()
