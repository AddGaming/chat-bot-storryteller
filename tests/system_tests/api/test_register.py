"""
Tests the functionality of the api
"""
import os
import unittest

import requests as rq

from utils.debug_tools import dump_html
from utils.decorators import create_test_env, load_package, boot_flask

DB_PATHS = [("one_admin_user.db", f"database{os.sep}userdata.db")]


class TestRegister(unittest.TestCase):

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_normal_register(self):
        rq_data = {
            "mail": "test_mail1",
            "pw1": "password",
            "pw2": "password"
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_normal_register")

        self.assertEqual(ans.status_code, 200)
        # TODO: check that permission lv is correct

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_admin_register(self):
        with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "r") as f:
            old_keys = f.readlines()
        ott = old_keys[0].replace("\n", "")

        rq_data = {
            "mail": "test_mail1",
            "pw1": "password",
            "pw2": "password",
            "ott": ott
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_admin_register")

        self.assertEqual(ans.status_code, 200)
        # TODO: check that permission lv is correct

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_wrong_ott(self):
        rq_data = {
            "mail": "test_mail1",
            "pw1": "password",
            "pw2": "password",
            "ott": "a"
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_wrong_ott")
        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_register_wo_pw(self):
        rq_data = {
            "mail": "test_mail1"
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_register_wo_pw")
        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_register_wo_matching_pws(self):
        rq_data = {
            "mail": "test_mail1",
            "pw1": "password1",
            "pw2": "password2"
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_register_wo_matching_pws")

        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_register_wo_mail(self):
        rq_data = {
            "pw1": "password",
            "pw2": "password"
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_register_wo_mail")

        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_to_short_mail(self):
        rq_data = {
            "mail": "t",
            "pw1": "password",
            "pw2": "password"
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_to_short_mail")

        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_long_mail(self):
        """
        to double-check if it can handle those
        """

        rq_data = {
            "mail": "test_mail" * 40,
            "pw1": "password",
            "pw2": "password"
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_long_mail")

        self.assertEqual(ans.status_code, 200)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_long_pw_200(self):
        """
        to double-check if it can handle those
        """

        rq_data = {
            "mail": "test_mail1",
            "pw1": "password" * 40,
            "pw2": "password" * 40
        }
        ans = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        dump_html(ans, "test_long_pw_200")

        self.assertEqual(ans.status_code, 200)


if __name__ == '__main__':
    unittest.main()
