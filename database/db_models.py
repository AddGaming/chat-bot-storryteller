"""
All models used in our 3 databases are described in here.
Changes in here might affect through the whole application.
Therefore, remember to test your changes first!
"""
import enum
from datetime import datetime

from flask_login import UserMixin
from sqlalchemy import Column, Integer, Text, DateTime, Boolean, String, ForeignKey
from sqlalchemy.orm import relationship

from database import _APPDB, _WORLDDB
from web_interface import _USER_DB


# USERS
class User(_USER_DB.Model, UserMixin):
    """
    User defining Data
    """
    __tablename__ = "users"
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    mail = Column(Text, unique=True, nullable=False)
    pw = Column(Text, nullable=False)
    date_of_creation = Column(DateTime(timezone=True), default=datetime.now)  # the date the user is created
    settings = relationship("UserSettingProfile", back_populates="user", passive_deletes=True)
    admin = Column(Boolean, default=False, nullable=False)
    world_id = Column(Integer, nullable=True)

    """
    def __dict__(self):
        return {
            "id": self.id,
            "mail": self.mail,
            "date_of_creation": self.date_of_creation,
            "admin": self.admin,
            "world_id": self.world_id
        }"""


class UserSettingProfile(_USER_DB.Model):
    """
    UserSettingProfile defining Data
    """
    __tablename__ = "user_settings"
    __table_args__ = {'extend_existing': True}  # should not happen in real Application. Is here for testing

    id = Column(Integer, primary_key=True)
    font_set = Column(String(20), nullable=False, default="default")  # the font family that the user chose.
    light_mode = Column(Boolean, default=False, nullable=False)

    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", back_populates="settings")

    """def __dict__(self):
        return {
            "id": self.id,
            "font_set": self.font_set,
            "light_mode": self.light_mode,
            "user_id": self.user_id
        }

    def __repr__(self):
        return f"{self.__dict__()}"""


# temporary workaround for server side sessions
class Session(_USER_DB.Model):
    """
    Session data
    """
    __tablename__ = "sessions"
    __table_args__ = {'extend_existing': True}


# APP DATA
class Ott(_APPDB):
    """
    OneTimeToken defining Data
    """
    __tablename__ = "otts"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    value = Column(Text, nullable=False)


class AccessInformation(_APPDB):
    """
    AccessInformation defining Data
    TODO: this is hella insecure!
    """
    __tablename__ = "access_information"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    app_id = Column(Text, default="")
    public_key = Column(Text, default="")
    permission_lv = Column(Text, default="")
    secret_key = Column(Text, default="")


class PlatformStatus(enum.Enum):
    """
    While not necessary to use, it helps with consistency in naming platform status.
    """
    ACTIVE = "active"
    DISCONNECTED = "disconnected"
    UNREGISTERED = "unregistered"


class Platform(_APPDB):
    """
    Platform defining Data
    """
    __tablename__ = "platforms"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    status = Column(String(20), nullable=False, default=PlatformStatus.UNREGISTERED)
    n_cmds_today = Column(Integer, default=0, nullable=False)
    n_cmds_week = Column(Integer, default=0, nullable=False)
    n_cmds_total = Column(Integer, default=0, nullable=False)
    n_nlu_today = Column(Integer, default=0, nullable=False)
    n_nlu_week = Column(Integer, default=0, nullable=False)
    n_nlu_total = Column(Integer, default=0, nullable=False)
    access_information = Column(ForeignKey("access_information.id"))
    nlu_active = Column(Boolean, default=False, nullable=False)


class Log(_APPDB):
    """
    Log defining Data
    """
    __tablename__ = "logs"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    message = Column(Text, default="", nullable=False)
    date = Column(DateTime(timezone=True), nullable=False, default=datetime.now)
    platform = Column(String(50), nullable=False, default="BackEnd")
    category = Column(Integer, default=0, nullable=False)


class Group(_APPDB):
    """
    Group defining Data
    """
    __tablename__ = "groups"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    trigger_symbol = Column(String(10), default="/")
    active = Column(Boolean, default=True, nullable=False)
    members = Column(Integer)  # id from Commands


class Command(_APPDB):
    """
    Command defining Data
    """
    __tablename__ = "commands"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False, unique=True)
    trigger_symbol = Column(String(10), default="/")
    group = Column(Integer, default=0, nullable=False)  # id from group
    author = Column(Integer, nullable=False)  # id from author
    date_of_modification = Column(DateTime(timezone=True), nullable=False, onupdate=datetime.now)
    date_of_creation = Column(DateTime(timezone=True), nullable=False, default=datetime.now)
    usages = Column(Integer, default=0, nullable=False)
    active = Column(Boolean, default=True, nullable=False)
    text_web = Column(Text, default="")
    text_discord = Column(String(2000), default="")
    text_telegram = Column(String(4090), default="")
    text_youtube = Column(String(200), default="")
    text_twitch = Column(String(460), default="")
    auto_delete = Column(Boolean, default=False, nullable=False)
    auto_delete_time = Column(Integer, default=60)

    def __dict__(self):
        return {
            "id": self.id,
            "name": self.name
            # TODO: continue
        }


class Input(_APPDB):
    """
    Input defining Data
    """
    __tablename__ = "inputs"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    text = Column(Text, nullable=False)
    categorized = Column(Boolean, default=False, nullable=False)
    expected_output = Column(Integer)  # ids from output table


class Output(_APPDB):
    """
    Output defining Data
    """
    __tablename__ = "outputs"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    text = Column(Text, nullable=False)
    author = Column(Integer, nullable=False)  # admin-user id


class Word(_APPDB):
    """
    Word defining Data
    """
    __tablename__ = "words"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    text = Column(Text, nullable=False)
    categorized = Column(Boolean, nullable=False, default=False)
    lexicographic = Column(Integer)  # id of a grammar obj
    semantic = Column(Integer)  # id of an entity


class Grammar(_APPDB):
    """
    Grammar defining Data
    """
    __tablename__ = "grammars"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)


class Entity(_APPDB):
    """
    Entity defining Data
    """
    __tablename__ = "entities"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)


# WORLD
class World(_WORLDDB):
    """
    Collection of data that makes up a world
    """
    __tablename__ = 'worlds'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    locations = relationship("Location", backref="locations")
    inhabitants = relationship("Character", backref="characters")
    players = relationship("Player", backref="players")

    def __str__(self):
        return f"{self.__tablename__}: {self.id=}\t{self.locations=}\t{self.inhabitants=}\t{self.players=}"


class Player(_WORLDDB):
    """
    Player defining Data
    """
    __tablename__ = 'players'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    world = Column(Integer, ForeignKey('worlds.id'))
    # a billion other parameters...


class FeatureCharacterTable(_WORLDDB):
    """
    linking table to make multi - multi connections possible
    """
    __tablename__ = "feature_character_association"
    __table_args__ = {'extend_existing': True}

    characters_id = Column(ForeignKey('characters.id'), primary_key=True)
    features_id = Column(ForeignKey('features.id'), primary_key=True)


class Character(_WORLDDB):
    """
    Character defining Data
    """
    __tablename__ = 'characters'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    call_name = Column(String(200), nullable=False)
    features = relationship("Feature", secondary=FeatureCharacterTable, back_populates="characters")
    phrases = relationship("Phrase", backref="phrases")
    current_phrase = Column(Integer, nullable=True)
    world_id = Column(Integer, ForeignKey('worlds.id'))
    # mood = None
    # connections = None


class Location(_WORLDDB):
    """
    Location defining Data
    """
    __tablename__ = 'locations'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False)
    characters = Column(Integer, ForeignKey('characters.id'))
    world_id = Column(Integer, ForeignKey('worlds.id'))


class Feature(_WORLDDB):
    """
    Feature defining Data
    """
    __tablename__ = 'features'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    description = Column(Text, default="")
    triggers = Column(Text, default="", nullable=True)  # ",".join(array) -> "..." -> "...".split(",") -> [...]
    characters = relationship("Character", secondary=FeatureCharacterTable, back_populates="features")
    # input_moods = None
    # output_moods = None


class Phrase(_WORLDDB):
    """
    Phrase defining Data
    """
    __tablename__ = 'phrases'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    re_entry = Column(Integer, nullable=True)
    text = Column(Text, default="")
    call_to_action = Column(Text, default="")
    quit = Column(Integer, nullable=True)
    next = Column(Integer, nullable=True)
    character = Column(Integer, ForeignKey('characters.id'))


"""
class Connection(_WORLDDB.Model):
    __tablename__ = 'connections'
    id = SQL.Column(Integer, primary_key=True)
    initiator = None
    receiver = None
    kind = None
"""
