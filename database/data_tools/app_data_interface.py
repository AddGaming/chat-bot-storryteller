"""
use this class to operate on user data. It serves as an interface and self administrates passive data
to reduce the function parameters
"""
import asyncio
import csv
import os
from dataclasses import dataclass

from sqlalchemy import desc, asc, func

from utils.crypto import check_pw


class AppDataInterface:
    """
    use this class to operate on user data. It serves as an interface and self administrates passive data
    to reduce the function parameters
    """

    @dataclass(order=True)
    class _Tracking:
        # the file representation of this class in json, not pickle because of RCE potential
        # class methods instead of functions for namespace reasons
        nlu: bool = False
        to_do_entities: int = 0
        to_do_intents: int = 0
        word_count: int = 0
        intent_count: int = 0
        entity_count: int = 0
        training_count: int = 0
        today_commands: int = 0
        today_nlu: int = 0
        week_commands: int = 0
        week_nlu: int = 0

        @staticmethod
        def schema():
            """
            The validation schema

            Returns: Json-Validation-Schema
            """
            return {
                "type": "object",
                "properties": {
                    "nlu": {"type": "boolean"},
                    "to_do_entities": {"type": "number"},
                    "to_do_intents": {"type": "number"},
                    "word_count": {"type": "number"},
                    "intent_count": {"type": "number"},
                    "entity_count": {"type": "number"},
                    "training_count": {"type": "number"},
                    "today_commands": {"type": "number"},
                    "today_nlu": {"type": "number"},
                    "week_commands": {"type": "number"},
                    "week_nlu": {"type": "number"}
                }
            }

        def save(self):
            """
            saves the cashed data into a json file
            """
            import json
            dump = json.dumps(self.__dict__)
            with open(f'{os.getcwd()}{os.sep}database{os.sep}tracking.json', "w") as f:
                f.write(dump)

        @classmethod
        def load(cls):
            """
            instantiate a new instance of the class from a json file. If the json file is broken in any way,
            it instead creates a new obj with default parameters

            Returns: _Tracking obj
            """
            # part of boot up dialog, therefore no db-logging
            import json
            from jsonschema import validate, exceptions

            try:
                with open(f'{os.getcwd()}{os.sep}database{os.sep}tracking.json', "r") as f:
                    content = json.loads(f.read())
                    # no validation needed, because unaccepted input_ is caught in loads() and __init__()
            except IOError:
                print("Trying to load non existing json -> instantiating a new version!")
                return cls()
            except json.decoder.JSONDecodeError:
                print("Trying to load a broken json -> instantiating a new version!")
                return cls()
            if content:
                try:
                    validate(content, cls.schema())
                except exceptions.ValidationError:
                    print("Trying to load a corrupt json -> instantiating a new version!")
                    return cls()
                return cls(
                    word_count=content['word_count'],
                    training_count=content['training_count'],
                    intent_count=content['intent_count'],
                    entity_count=content['entity_count'],
                    to_do_intents=content['to_do_intents'],
                    to_do_entities=content['to_do_entities'],
                    nlu=content['nlu'])
            else:
                print("Trying to load a corrupt json -> instantiating a new version!")
                return cls()

    def __init__(self, db_session=None):
        if db_session is None:
            from database import app_session
            db_session = app_session

        self.db_session = db_session

        if not os.path.exists(f"{os.getcwd()}{os.sep}database{os.sep}database{os.sep}tracking"):
            self.tracking = self._Tracking()
        else:
            self.tracking = self._Tracking.load()

    def __str__(self):
        return f"{self.db_session=}"

    async def __generate_ott(self, amount: int, file_save: bool):
        from utils.crypto import time_hash, hash_pw_async
        from ..db_models import Ott

        raw_otts = []
        for _ in range(amount):
            raw_otts.append(time_hash())

        if file_save:
            with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "a") as file:
                for ott in raw_otts:
                    file.write(f"{ott}\n")

        hashes = await asyncio.gather(*[hash_pw_async(ott) for ott in raw_otts])
        otts = [Ott(value=hash_) for hash_ in hashes]
        for e in otts:
            self.db_session.add(e)
        self.db_session.commit()
        # TODO: enable when frontend connected
        # flash(ott, "success")

    def generate_otts(self, amount: int = 1, file_save: bool = True) -> None:
        """
        generates new one-time-tokens. It deletes all existing tokens before creating new ones.
        The new tokens get saved in a txt file. If this file is not needed disable in the kwargs.
        Else remember to delete this file after extracting it to a save place.

        Args:
            file_save: If a txt with the clear-text-otts should be written
            amount: how many otts should be generated
        """
        from database import Ott

        for ott in self.db_session.query(Ott).all():
            self.db_session.delete(ott)
        self.db_session.commit()

        # otts get written into a file unencrypted. so make sure to delete the file after copying the keys
        if file_save:
            with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "w") as file:
                file.write("")

        asyncio.run(self.__generate_ott(amount, file_save))

    async def __check_ott(self, raw: str) -> bool:
        from ..db_models import Ott

        otts = self.db_session.query(Ott).all()

        results = await asyncio.gather(*[check_pw(raw, ott.value) for ott in otts])
        results = zip(results, otts)

        for res in results:
            if res[0]:
                self.db_session.delete(res[1])
                self.db_session.commit()
                return True
        return False

    def check_ott(self, raw: str) -> bool:
        """
        checks if the ott exists. If one is found, it is also deleted.

        Args:
            raw: str - the plaintext ott

        Returns: bool - True if found and deleted, False if not found
        """
        # since every check should only succeed once, this also deletes the positive results

        return asyncio.run(self.__check_ott(raw))

    def count_ott(self) -> int:
        """
        counts how many otts are currently in usable

        Returns: int
        """
        from ..db_models import Ott
        amount = 0
        # for loop since to small and rare to make performance impact + deduplication entries
        for entry in self.db_session.query(Ott).all():
            amount += 1
            print(f"{entry.id= }\n{entry.value}")
        self.db_session.close()
        return amount

    def delete_ott_file(self):
        """
        Deletes the plain text ott file on disc

        Returns: None
        """
        if 'otts.txt' in os.listdir(f"{os.getcwd()}{os.sep}database"):
            os.remove(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt")

    async def top_commands_3(self):
        """
        Returns: the 3 most used commands. If not enough commands are registered,
            <None> will get returned as the command replacement
        """
        from ..db_models import Command
        descending = self.db_session.query(Command).order_by(desc(Command.usages))
        llist = descending[0:3]

        while len(llist) < 3:
            llist.append(None)

        return llist

    # TODO: potential for parametrization since code duplication with above
    async def flop_commands_3(self):
        """
        Returns: the 3 most unused commands. If not enough commands are registered,
            <None> will get returned as the command replacement
        """
        from ..db_models import Command
        descending = self.db_session.query(Command).order_by(asc(Command.usages))
        llist = descending[0:3]

        while len(llist) < 3:
            llist.append(None)

        return llist

    # TODO: test this
    async def analyse_calls(self) -> dict:
        """
        Analysis the database in regard to bot-calls that got placed
        it includes: "total", "week", "today", "total_commands", "week_commands", "today_commands", "total_nlu",
        "week_nlu", "today_nlu"
        as well as a platform specific dict that's available under the platform name as key

        Returns: a dictionary with all the important analysis data
        """
        from ..db_models import Platform
        platform_data = self.db_session.query(Platform).all()
        ret = {
            "total": 0, "week": 0, "today": 0, "total_commands": 0, "week_commands": 0, "today_commands": 0,
            "total_nlu": 0, "week_nlu": 0, "today_nlu": 0
        }
        for platform in platform_data:
            today = (platform.n_commands_today, platform.n_nlu_today)
            week = (platform.n_commands_week, platform.n_nlu_week)
            total = (platform.n_commands_total, platform.n_nlu_total)
            ret[platform.name] = {
                "today": today,
                "week": week,
                "total": total,
                "%": None}
            ret["total"] += total[0] + today[1]
            ret["week"] += week[0] + week[1]
            ret["today"] += today[0] + today[1]
            ret["total_commands"] += total[0]
            ret["week_commands"] += week[0]
            ret["today_commands"] += today[0]
            ret["total_nlu"] += today[1]
            ret["week_nlu"] += week[1]
            ret["today_nlu"] += today[1]
        for platform in platform_data:
            try:
                percent = (ret[platform.name][0][0] + ret[platform.name][0][1]) / (ret["total"] / 100)
            except ZeroDivisionError:
                percent = 0
            ret[platform.name]["%"] = percent
        return ret

    # TODO: test this
    async def update_command(self, dic: dict, id_: int = None):
        """
        This function updates a command given by id

        Args:
            dic: a dict with all the key-val pairs that you want to update
            id_: optional - the id of the command you want to update. If given as param and in dict, the param is used
            and the key-val pair is discarded

        Returns: None
        """
        from database.data_tools.data_validation import validate_command
        from database import Command
        from database import LOGGER
        if not validate_command(dic):
            LOGGER.log(
                f"The command data on which to update is corrupt: {dic=}",
                category=LOGGER.LogCategory.WARNING)
            # TODO: Flash this once pages connected
            return

        if id_:
            if "id" in dic:
                del dic["id"]
        else:
            try:
                id_ = dic["id"]
            except KeyError:
                LOGGER.log(
                    "The command to update has no ID to identify it", category=LOGGER.LogCategory.WARNING)
                # TODO: Flash this once pages connected
                return

        self.db_session.query(Command).filter_by(Command.id == id_).update(dic)
        # alternative if above fails
        # cmd = Command.query().filter_by(Command.id == id_)
        # setattr(cmd, 'key', val)
        self.db_session.commit()

    def create_command(self):
        # Validate
        # TODO: Implement this
        pass

    def create_group(self):
        # Validate
        # TODO: Implement this
        pass

    def update_group(self):
        # Validate new group information
        # TODO: Implement this
        pass

    def update_nlu_response(self):
        from database.data_tools.data_validation import validate_nlu_response
        validate_nlu_response()
        # TODO: Implement this
        pass

    def update_nlu_input(self):
        # TODO: Implement this
        pass

    def update_nlu_entity(self):
        from database.data_tools.data_validation import validate_nlu_entity
        validate_nlu_entity()
        # TODO: Implement this
        pass

    def update_nlu_word(self, id_: int = None, old_text: str = "", new_text: str = "") -> None:
        # TODO: Implement this
        pass

    # TODO: Test this
    # TODO: support date of creation read-in
    async def parse_csv_to_commands(self, csv_s: str = "") -> None:
        """
        takes a csv string and turns it into commands asynchronously.
        No return. It automatically writes it into db if valid csv gets passed.
        <Date of creation> and <date of modification> will not be read and instead set to the current time.

        Args:
            csv_s: a string of a csv-file
        Returns:
            None
        """
        from database import LOGGER
        from database import Command
        from database.data_tools.data_validation import validate_command_csv
        if not validate_command_csv(csv_s):
            # TODO: Flash this once pages connected
            LOGGER.log("Invalid Command-CSV submitted", LOGGER.LogCategory.WARNING)
            return
        reader = csv.DictReader(csv_s.splitlines())
        for row in reader:
            # formatting
            row["id"] = int(row["id"])
            row["group"] = int(row["group"])
            row["author"] = int(row["author"])
            del row["date_of_modification"]
            del row["date_of_creation"]
            row["usages"] = int(row["usages"])
            if row["active"] == "False":
                row["active"] = False
            else:
                row["active"] = True
            if row["auto_delete"] == "False":
                row["auto_delete"] = False
            else:
                row["auto_delete"] = True
            row["auto_delete_time"] = int(row["auto_delete_time"])

            # insertion
            cmd = Command(**row)
            self.db_session.add(cmd)
            self.db_session.commit()
        # TODO: Flash success once pages connected

    # TODO: Test this
    async def parse_csv_to_outputs(self, csv_s: str = "") -> None:
        """
        takes a csv string and turns it into outputs asynchronously.
        No return. It automatically writes it into db if valid csv gets passed.

        Args:
            csv_s: a string of a csv-file
        Returns:
            None
        """
        from database import LOGGER
        from database.data_tools.data_validation import validate_output_csv
        from ..db_models import Output
        if not validate_output_csv(csv_s):
            # TODO: Flash this once pages connected
            LOGGER.log("Invalid Output-CSV submitted", LOGGER.LogCategory.WARNING)
            return
        reader = csv.DictReader(csv_s.splitlines())
        for row in reader:
            # formatting
            row["id"] = int(row["id"])
            row["author"] = int(row["author"])

            # insertion
            opt = Output(**row)
            self.db_session.add(opt)
            self.db_session.commit()
        # TODO: Flash success once pages connected

    # TODO: test this
    def parse_csv_to_input(self, csv_s: str = "") -> None:
        """
        takes a csv string and turns it into inputs asynchronously.
        No return. It automatically writes it into db if valid csv gets passed.

        Args:
            csv_s: a string of a csv-file
        Returns:
            None
        """
        from database import LOGGER
        from database.data_tools.data_validation import validate_input_csv
        from ..db_models import Input
        if not validate_input_csv(csv_s):
            # TODO: Flash this once pages connected
            LOGGER.log("Invalid Input-CSV submitted", LOGGER.LogCategory.WARNING)
            return
        reader = csv.DictReader(csv_s.splitlines())
        for row in reader:
            # formatting
            row["id"] = int(row["id"])
            if row["categorized"] == "False":
                row["categorized"] = False
            else:
                row["categorized"] = True
            row["expected_output"] = int(row["expected_output"])
            # insertion
            ipt = Input(**row)
            self.db_session.add(ipt)
            self.db_session.commit()
        # TODO: Flash success once pages connected

    # TODO: test this
    def parse_csv_to_entity(self, csv_s: str = "") -> None:
        """
        takes a csv string and turns it into entities asynchronously.
        No return. It automatically writes it into db if valid csv gets passed.

        Args:
            csv_s: a string of a csv-file
        Returns:
            None
        """
        from ..db_models import Entity
        from database import LOGGER
        from database.data_tools.data_validation import validate_entity_csv
        if not validate_entity_csv(csv_s):
            # TODO: Flash this once pages connected
            LOGGER.log("Invalid Entity-CSV submitted", LOGGER.LogCategory.WARNING)
            return
        reader = csv.DictReader(csv_s.splitlines())
        for row in reader:
            # formatting
            row["id"] = int(row["id"])

            # insertion
            entity = Entity(**row)
            self.db_session.add(entity)
            self.db_session.commit()
        # TODO: Flash success once pages connected

    # TODO: test this
    def parse_csv_to_words(self, csv_s: str = "") -> None:
        """
        takes a csv string and turns it into words asynchronously.
        No return. It automatically writes it into db if valid csv gets passed.

        Args:
            csv_s: a string of a csv-file
        Returns:
            None
        """
        from ..db_models import Word
        from database import LOGGER
        from database.data_tools.data_validation import validate_word_csv
        if not validate_word_csv(csv_s):
            # TODO: Flash this once pages connected
            LOGGER.log("Invalid Word-CSV submitted", LOGGER.LogCategory.WARNING)
            return
        reader = csv.DictReader(csv_s.splitlines())
        for row in reader:
            # formatting
            row["id"] = int(row["id"])
            row["lexicographic"] = int(row["lexicographic"])
            row["semantic"] = int(row["semantic"])
            if row["categorized"] == "False":
                row["categorized"] = False
            else:
                row["categorized"] = True
            # insertion
            word = Word(**row)
            self.db_session.add(word)
            self.db_session.commit()
        # TODO: Flash success once pages connected

    def update_tracking(self, new_data=None):
        # TODO: fix
        from database.data_tools.data_validation import validate_application_data
        validate_application_data(new_data)
        from database.db_models import Word, Entity, Input, Output
        new_words = []
        all_words = []
        for w in Word.query.all():
            if w.status == "to_do" or w.status == "uncategorized":
                new_words.append(w)
            all_words.append(w)
        self.tracking.to_do_entities = len(new_words)
        self.tracking.word_count = len(all_words)

        new_questions = []
        training_questions = []
        for q in Input.query.all():
            if q.status == "to_do" or q.status == "uncategorized":
                new_questions.append(q)
            elif q.status == "categorized":
                training_questions.append(q)

        self.tracking.to_do_intents = len(new_questions)
        self.tracking.training_count = len(training_questions)

        all_answers = []
        for a in Output.query.all():
            all_answers.append(a)
        self.tracking.intent_count = len(all_answers)

        all_entities = []
        for e in Entity.query.all():
            all_entities.append(e)
        self.tracking.entity_count = len(all_entities)

    async def get_platform_status(self):
        from ..db_models import Platform
        result = {}
        for platform in self.db_session.query(Platform).all():
            result[platform.name] = platform.status
        return result

    def export_db_to_csvs(self):
        # TODO: Implement this
        pass

    def parse_folder_to_db(self):
        # TODO: Implement this
        pass

    async def __list_logs(self):
        """
        Returns: a list with all log data as tuples
        """
        from ..db_models import Log
        x = []
        for e in self.db_session.query(Log).all():
            x.append(e)
        self.db_session.close()
        return x

    def list_logs(self):
        """
        Returns: a list with all log data as tuples
        """
        return asyncio.run(self.__list_logs())
