import enum

from utils.errors import MissingArgument, InvalidArgumentConfiguration


# TODO: Test TextEffectSyntax
class TextEffectSyntax:
    """
    This class is a namespace for every function/data that you need to change syntax in context
    of this project
    """
    class Syntax(enum.Enum):
        DISCORD = {
            "bold": "**",
            "italic": "*",
            "underline": "__",
            "crossed_out": "~~",
            "code": "`"
        }

        TELEGRAM = {
            "bold": "**",
            "italic": "*",
            "underline": "",
            "crossed_out": "~~",
            "code": "`"
        }
        YOUTUBE = {
            "bold": "",
            "italic": "",
            "underline": "",
            "crossed_out": "",
            "code": ""
        }
        TWITCH = {
            "bold": "",
            "italic": "",
            "underline": "",
            "crossed_out": "",
            "code": ""
        }
        HTML = {
            "bold": "b",
            "italic": "i",
            "underline": "ins",
            "crossed_out": "del",
            "code": "code"
        }

    def _to_html(self, text: str, original: Syntax):

        for key, mark in original.value.items():
            a = text.split(mark)
            b = []
            opening = True
            for e in a:
                b.append(e)
                if opening:
                    b.append(f"<{self.Syntax.HTML.value[key]}>")
                    opening = False
                else:
                    b.append(f"<{self.Syntax.HTML.value[key]}/>")
                    opening = False

            b.pop(len(b) - 1)
            text = "".join(b)

        return text

    @staticmethod
    def switch_syntax(text: str, original: Syntax, target: Syntax):
        from database import LOGGER
        self = TextEffectSyntax()
        # catching invalid params
        if not text:
            LOGGER.log(text="Translating Syntax-marker on an empty text", category=LOGGER.LogCategory.WARNING)
            return ""
        if target != self.Syntax.HTML:
            for key in original.value:
                try:
                    target.value[key]
                except KeyError:
                    LOGGER.log(f"Syntax of translation is not properly defined! {key=}, {target.value[key]}")
                    raise KeyError("The syntax conversion is not defined correctly. Not matching keys")

        # function body
        # TODO: missing from html to normal!
        if target != self.Syntax.HTML:
            print(original.value)
            for key, mark in original.value.items():
                if mark:  # checks if a syntax was defined for this key
                    text = text.replace(mark, target.value[key])
            return text
        else:
            return self._to_html(text, original)

    def cascade(self, texts=None):
        from database import LOGGER
        if not texts:
            LOGGER.log("", category=LOGGER.LogCategory.WARNING)
            raise MissingArgument(f"The texts to cascade are invalid: {texts}")

        for stx, msg in texts:
            match stx.lower():
                case "discord":
                    pass
                case "html":
                    for key, text in texts:
                        pass
                case "telegram":
                    pass
                case "twitch":
                    pass
                case "youtube":
                    pass


# TODO: test this
async def evaluate_command_str(
        raw_text: str = "", cmd_id: int = None, platform: int = None) -> str:
    """
    This function evaluates the raw cmd string to enable command nesting and calls as well as variable evaluation.
    There are 3 ways to use this function:
    (raw_text) -> returns the evaluation for web,
    (raw_text, platform) -> returns evaluation for platform,
    (cmd_id, platform) -> evaluates the text of the given command id for the specified platform
    Any other way of using this function will result in an error

    Args:
        raw_text: the raw command text
        cmd_id: the id of command which to use
        platform: the platform

    Returns: evaluated command str
    """
    from database import APPDATA, Commands, LOGGER
    # invalid inputs
    # TODO: check call safety to potentially remove redundant checks
    if not platform:
        platform = LOGGER.Platforms.APPLICATION  # default replacement is for web usage
    if not raw_text and not cmd_id:
        raise InvalidArgumentConfiguration("Evaluate_command_str() is missing either a raw_text or an cmd_id!")
    if raw_text and cmd_id:
        raise InvalidArgumentConfiguration(
            "Evaluate_command_str() received both a raw_text and an cmd_id. It expected only one of them!")
    # getting text from cmd
    if cmd_id:
        raw_cmd = None  # declaration to supress error
        cmd_cursor = await APPDATA._async_db_execute(Commands.select().where(Commands.c.id == cmd_id))
        for e in cmd_cursor:  # since cursor has always exactly 1 elem, but is not subscribable
            raw_cmd = e

        match platform:
            case LOGGER.Platforms.TELEGRAM.value:
                raw_text = raw_cmd.text_telegram
            case LOGGER.Platforms.DISCORD.value:
                raw_text = raw_cmd.text_discord
            case LOGGER.Platforms.TWITCH.value:
                raw_text = raw_cmd.text_twitch
            case LOGGER.Platforms.YOUTUBE.value:
                raw_text = raw_cmd.text_youtube
            case LOGGER.Platforms.APPLICATION.value:
                raw_text = raw_cmd.text_web
            case _:
                raise ValueError(f"Evaluate_command_str() expected a known platform, got {platform}")

    # evaluating text
    # TODO: optimize this
    text = ""
    commands_cursor = await APPDATA._async_db_execute(Commands.select())
    match platform:
        case LOGGER.Platforms.TELEGRAM.value:
            for command in commands_cursor:
                text = text.replace((command.symbol + command.name), command.text_telegram)
            return text
        case LOGGER.Platforms.DISCORD.value:
            for command in commands_cursor:
                text = text.replace((command.symbol + command.name), command.text_discord)
            return text
        case LOGGER.Platforms.TWITCH.value:
            for command in commands_cursor:
                text = text.replace((command.symbol + command.name), command.text_twitch)
            return text
        case LOGGER.Platforms.YOUTUBE.value:
            for command in commands_cursor:
                text = text.replace((command.symbol + command.name), command.text_youtube)
            return text
        case LOGGER.Platforms.APPLICATION.value:
            for command in commands_cursor:
                text = text.replace((command.symbol + command.name), command.text_web)
            return text
        case _:
            raise ValueError(f"Evaluate_command_str() expected a known platform, got {platform}")


if __name__ == "__main__":
    myText = "~~LOL~~__XD__"
    newText = TextEffectSyntax.switch_syntax(
        myText, TextEffectSyntax.Syntax.DISCORD, TextEffectSyntax.Syntax.TELEGRAM)
    print(newText)
    newText = TextEffectSyntax.switch_syntax(
        myText, TextEffectSyntax.Syntax.DISCORD, TextEffectSyntax.Syntax.HTML)
    print(newText)
