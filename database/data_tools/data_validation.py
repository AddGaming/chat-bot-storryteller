"""
All data validation operations are defined in here
"""
import csv
from utils.errors import *


# TODO: test this
def validate_command_csv(csv_s: str = "") -> bool:
    """
    validates the correctness of a given csv-string in regard to the command table

    Args:
        csv_s: the string representation of a csv
    Returns: bool
    """
    lines = csv_s.splitlines()
    head = lines[0]
    body = csv.DictReader(lines)
    fieldnames = ["id", "name", "trigger_symbol", "group", "author", "date_of_modification", "date_of_creation",
                  "usages", "active", "test_web", "text_discord", "text_telegram", "text_youtube", "text_twitch",
                  "auto_delete", "auto_delete_time"]
    # test correct header
    for f in fieldnames:
        if not (f in head):
            return False
    # check type correctness
    for row in body:
        if not validate_command(row):
            return False
    return True


# TODO: test this
def validate_output_csv(csv_s: str = "") -> bool:
    """
    validates the correctness of a given csv-string in regard to the Outputs table

    Args:
        csv_s: the string representation of a csv
    Returns: bool
    """
    lines = csv_s.splitlines()
    head = lines[0]
    body = csv.DictReader(lines)
    fieldnames = ["id", "text", "author"]
    # test correct header
    for f in fieldnames:
        if not (f in head):
            return False
    # check type correctness
    for row in body:
        for key, val in row:
            match key:
                case "id":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case "author":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case "text":
                    pass
                case _:  # unknown key value
                    return False
    return True


# TODO: test this
def validate_input_csv(csv_s: str = "") -> bool:
    """
    validates the correctness of a given csv-string in regard to the Inputs table

    Args:
        csv_s: the string representation of a csv
    Returns: bool
    """
    lines = csv_s.splitlines()
    head = lines[0]
    body = csv.DictReader(lines)
    fieldnames = ["id", "text", "categorized", "expected_output"]
    # test correct header
    for f in fieldnames:
        if not (f in head):
            return False
    # check type correctness
    for row in body:
        for key, val in row:
            match key:
                case "id":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case "categorized":
                    if val != "True" and val != "False":
                        return False
                case "text":
                    pass
                case "expected_output":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case _:  # unknown key value
                    return False
    return True


# TODO: test this
def validate_entity_csv(csv_s: str = "") -> bool:
    """
    validates the correctness of a given csv-string in regard to the Entities table

    Args:
        csv_s: the string representation of a csv
    Returns: bool
    """
    lines = csv_s.splitlines()
    head = lines[0]
    body = csv.DictReader(lines)
    fieldnames = ["id", "name"]
    # test correct header
    for f in fieldnames:
        if not (f in head):
            return False
    # check type correctness
    for row in body:
        for key, val in row:
            match key:
                case "id":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case "name":
                    pass
                case _:  # unknown key value
                    return False
    return True


# TODO: test this
def validate_word_csv(csv_s: str = "") -> bool:
    """
    validates the correctness of a given csv-string in regard to the Words table

    Args:
        csv_s: the string representation of a csv
    Returns: bool
    """
    lines = csv_s.splitlines()
    head = lines[0]
    body = csv.DictReader(lines)
    fieldnames = ["id", "text", "categorized", "lexicographic", "semantic"]
    # test correct header
    for f in fieldnames:
        if not (f in head):
            return False
    # check type correctness
    for row in body:
        for key, val in row:
            match key:
                case "id":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case "lexicographic":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case "semantic":
                    try:
                        int(val)
                    except ValueError:
                        return False
                case "categorized":
                    if val != "True" and val != "False":
                        return False
                case "text":
                    pass
                case _:  # unknown key value
                    return False
    return True


# TODO: Test this
def validate_command(dic: dict) -> bool:
    """
    takes a command's data as a dictionary and checks of a command of this structure can be constructed
    this function also lets incomplete dictionaries pass, so be sure to catch missing/required parameters elsewhere

    Args:
        dic: The dictionary which constitutes a command

    Returns: bool
    """
    for key, val in dic:
        match key:
            case "id":
                try:
                    int(val)
                except ValueError:
                    return False
            case "name":
                if len(val) > 200:
                    return False
            case "trigger_symbol":
                if len(val) > 10:
                    return False
            case "group":
                try:
                    int(val)
                except ValueError:
                    return False
            case "author":
                try:
                    int(val)
                except ValueError:
                    return False
            case "date_of_modification":
                pass
            case "date_of_creation":
                pass
            case "usages":
                try:
                    int(val)
                except ValueError:
                    return False
            case "active":
                if val != "True" and val != "False":
                    return False
            case "test_web":
                pass
            case "text_discord":
                if len(val) > 2000:
                    return False
            case "text_telegram":
                if len(val) > 4090:
                    return False
            case "text_youtube":
                if len(val) > 200:
                    return False
            case "text_twitch":
                if len(val) > 460:
                    return False
            case "auto_delete":
                if val != "True" and val != "False":
                    return False
            case "auto_delete_time":
                try:
                    int(val)
                except ValueError:
                    return False
            case _:  # unknown key value
                return False
    return True


def validate_nlu_response(text=""):
    # TODO: Implement this
    pass


def validate_nlu_entity(name="", values=None):
    # TODO: Implement this
    pass


def validate_application_data(dic=None):
    # TODO: Implement this
    pass


def validate_login_data(name="", password=""):
    """ makes sure that our Login only Accepts legal Mails and Passwords"""
    # check for SQl_Injection
    legal_letters = "1234567890ßqwertzuiopüasdfghjklöäyxcvbnmQWERTZUIOPÜASDFGHJKLÖÄYXCVBNM.-_@"
    if " " in name or " " in password:
        raise IllegalArgument("Email or Password contain Whitespace")
    for letter in name:
        if letter not in legal_letters:
            raise IllegalArgument("Email contains illegal letters")
    for letter in password:
        if letter not in legal_letters:
            raise IllegalArgument("Password contains illegal letters")


def validate_chat_data(text=""):
    """ makes sure that the Text from the User is nothing more that a Phrase to send to Dialogflow"""
    legal_letters = "1234567890ßqwertzuiopüasdfghjklöäyxcvbnmQWERTZUIOPÜASDFGHJKLÖÄYXCVBNM.-_@!? "
    for letter in text:
        if letter not in legal_letters:
            raise IllegalArgument("Chatmessage contained illegal letters")
