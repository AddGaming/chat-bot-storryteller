# this class administers interaction on the world data
from utils.Queue import ThreadQueue


class WorldDataInterface:

    def __init__(self, threads=5, db_connection=None):
        if db_connection is None:
            from database import app_session
            db_connection = app_session
        self.queue = ThreadQueue(threads)
        self.queue.run()
        self.connection = db_connection

    def world_create(self):
        # TODO: Implement this
        pass

    def world_delete(self):
        # TODO: Implement this
        pass
