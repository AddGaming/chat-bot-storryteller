"""
The exposed end of the database.
Every ever relevant information for the outside to see is exposed from here
This init also handles setup if needed
"""
import importlib
import os
import sys

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base

from .data_tools.app_data_interface import AppDataInterface


_APPDB = declarative_base()
_WORLDDB = declarative_base()

# Wildcard import since the module will be accessed from outside. not the files inside the modules
# all the models must be imported before the .db is created. else they ain't included
from .db_models import *

# package globals

app_data_engine = create_engine(f'sqlite:///{os.getcwd()}{os.sep}database{os.sep}appdata.db')
user_data_engine = create_engine(f'sqlite:///{os.getcwd()}{os.sep}database{os.sep}userdata.db')
world_data_engine = create_engine(f'sqlite:///{os.getcwd()}{os.sep}database{os.sep}worlddata.db')

# check for existing db's
if not os.path.exists(f"{os.getcwd()}{os.sep}database{os.sep}appdata.db"):
    if os.getcwd().split(os.sep)[-1] != "chat-bot-storyteller":  # not deployment but test env
        mod = sys.modules['database.db_models']
        importlib.reload(mod)
    print("no appdata table found, creating new...")
    _APPDB.metadata.create_all(app_data_engine)

if not os.path.exists(f"{os.getcwd()}{os.sep}database{os.sep}worlddata.db"):
    if os.getcwd().split(os.sep)[-1] != "chat-bot-storyteller":
        mod = sys.modules['database.db_models']
        importlib.reload(mod)
    print("no worlddata table found, creating new...")
    _WORLDDB.metadata.create_all(world_data_engine)

# create db_session objs
app_session = sessionmaker(bind=app_data_engine)()  # create a new obj directly since the class is never used elsewhere
world_session = sessionmaker(bind=world_data_engine)()

APPDATA = AppDataInterface(db_session=app_session)
WORLDDATA = None

from utils.db_logger import DBLogger

LOGGER = DBLogger(log_lv=DBLogger.LogCategory.DEBUG)

LOGGER.log("Finished database initialization")

if not ('otts.txt' in os.listdir(f"{os.getcwd()}{os.sep}database")):
    APPDATA.generate_otts(amount=2)
