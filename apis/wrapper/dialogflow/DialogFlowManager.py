"""
This class manages api calls to dialogflow
This is done inside a class due to the inconvenience to state and variable management.
"""
import os
import time

import google.cloud.dialogflow_v2 as dialogflow_vx
from .DialogFlowSettings import ALLOWED_LANGUAGES
from utils.errors import *


class DialogFlowManager:  # rename to dlfapi? because it only does that.
    """
    This class manages everything with dialogflow
    """

    def __init__(self, project_id="", lang_code="de"):
        if project_id == "":
            raise RuntimeError("No project ID given for DialogFlowManager")
        if lang_code not in ALLOWED_LANGUAGES:
            raise RuntimeError("Language not supported for DialogFlowManager")
        path = __file__.split(os.sep)[:-1]
        path.append("gcloud-api-adminkey.json")
        gcloud_api_key = os.sep.join(path)
        self.AgentClient = dialogflow_vx.AgentsClient.from_service_account_file(gcloud_api_key)
        self.ContextClient = dialogflow_vx.ContextsClient.from_service_account_file(gcloud_api_key)
        self.EntityClient = dialogflow_vx.EntityTypesClient.from_service_account_file(gcloud_api_key)
        self.IntentClient = dialogflow_vx.IntentsClient.from_service_account_file(gcloud_api_key)
        self.SessionClient = dialogflow_vx.SessionsClient.from_service_account_file(gcloud_api_key)
        self.ProjectID = project_id
        self.ProjectPath = "projects/" + self.ProjectID
        self.AgentPath = self.AgentClient.agent_path(project=self.ProjectID)
        self.Language = lang_code

    def __str__(self):
        llist = []
        for entry in self.list_intents():
            llist.append(entry)
        r = f'linked_agents: {len(self.list_agents())}\n'
        r += f'registered_entity_types: {len(self.list_entity_types())}\n'
        r += f"registered_intents: {len(llist)}\n"
        return r

    def get_agent(self):
        """Returns the Dialogflow-Agent linked to this Project \n
        :returns: DialogFlow-Agent"""
        # since a agents is always linked to 1 project only this works as long as there is a agent registered for the
        # project
        return self.AgentClient.get_agent(parent=self.ProjectPath)

    def train_agent(self):
        """issues a training command to the linked agent to train him on the uploaded intents"""
        req = dialogflow_vx.TrainAgentRequest()
        req.parent = self.ProjectPath
        # TODO: Test the return type and include in docstring
        return self.AgentClient.train_agent(request=req)

    def list_entity_types(self):
        """:returns: a list of all DialogFlow-EntityTypes for the registered Agent/Project"""
        ret = []
        for elem in self.EntityClient.list_entity_types(language_code=self.Language, parent=self.AgentPath):
            ret.append(elem)
        return ret

    def list_intents(self):
        """:returns: a list of DialogFlow-Intents"""
        return dialogflow_vx.IntentsClient.list_intents(self=self.IntentClient, parent=self.AgentPath,
                                                        language_code=self.Language)

    def update_entity_types(self, entity_type_name="", entities_to_add=None, entities_to_remove=None):
        """Updates an entity type by adding or removing entities
                :param entity_type_name: str (display name of the entity type)
                :param entities_to_add: [str] (the entities we want to add to the type, if any)
                :param entities_to_remove: [str] (the entities we want to remove from the type, if any)
                :returns: ?"""
        if entity_type_name == "":
            raise MissingArgument(f"Not all necessary parameters filled sufficiently: \n {entity_type_name=}")
        if not entities_to_add:
            entity_list = []
            for entity in entities_to_add:
                entity_list.append(
                    dialogflow_vx.EntityType.Entity(value=entity, synonyms=entity)
                )
            self.EntityClient.batch_update_entities(parent=entity_type_name, entities=entity_list)
        if entities_to_remove is not None:
            self.EntityClient.batch_delete_entities(parent=entity_type_name, entity_values=entities_to_remove)

    def batch_upload_entity_types(self, entity_types_to_create=None):
        """loads the entity types to the dialogflow agent
                        :param entity_types_to_create: [Entity_Type] (List of Entity Types to upload)
                        :returns: ?"""
        if not entity_types_to_create:
            raise MissingArgument(f"Not all necessary parameters filled sufficiently: \n {entity_types_to_create=}")

        entity_type_batch = dialogflow_vx.EntityTypeBatch()
        entity_type_batch.entity_types = entity_types_to_create
        req_data = dialogflow_vx.BatchUpdateEntityTypesRequest()
        req_data.entity_type_batch_inline = entity_type_batch
        req_data.language_code = self.Language
        req_data.parent = self.AgentPath
        return self.EntityClient.batch_update_entity_types(request=req_data)

    def batch_upload_intents(self, intents_to_create=None):
        """loads the intents to the dialogflow agent
                        :param intents_to_create: [Intent] (List of Intents to upload)
                        :returns: ?"""
        if not intents_to_create:
            raise MissingArgument(f"Not all necessary parameters filled sufficiently: \n {intents_to_create=}")

        intent_batch = dialogflow_vx.IntentBatch()
        intent_batch.intents = intents_to_create
        req_data = dialogflow_vx.BatchUpdateIntentsRequest()
        req_data.intent_batch_inline = intent_batch
        req_data.language_code = self.Language
        req_data.parent = self.AgentPath
        return self.IntentClient.batch_update_intents(request=req_data)

    def respond(self, input_="", session_id=None, contexts=None):
        """
        gets the DialogFlow-Response to a given input_
        :param input_: str (the input_)
        :param session_id: str (a session id under which the conversation is held, will be linked to users later)
        :param contexts: [str] (a list of the Context Names we want our Session to have at the moment)
        :returns: str
        """
        # is only here since we dont have users yet
        if not session_id:
            c_time = str(time.time())
            session_id = abs(int(hash(c_time)))
        session_path = self.AgentClient.agent_path("storyteller-bot") + "/sessions/" + str(session_id)
        context_path = session_path + "/contexts/"
        # add all contexts to the session
        if not not contexts:
            for context in contexts:
                dialogflow_context = dialogflow_vx.Context()
                dialogflow_context.name = context_path + context
                dialogflow_context.lifespan_count = 1
                self.ContextClient.create_context(context=dialogflow_context, parent=session_path)

        text_input = dialogflow_vx.TextInput(text=input_, language_code="de")
        query_input = dialogflow_vx.QueryInput(text=text_input)
        req = dialogflow_vx.DetectIntentRequest(session=session_path, query_input=query_input)
        resp = self.SessionClient.detect_intent(request=req)
        formatted_resp = resp.query_result.fulfillment_text
        self.ContextClient.delete_all_contexts(parent=session_path)
        return formatted_resp
