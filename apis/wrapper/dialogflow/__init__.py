"""
defines outward facing interface of implemented functionality in this module
# TODO: doubling of documentation - find a fix for this that works with the ide
"""

from .dialogflow_functions import *
from .DialogFlowManager import DialogFlowManager

MANAGER = DialogFlowManager(project_id="storyteller-bot")  # replace this with your project id


def get_agent():
    """Returns the Dialogflow-Agent linked to this Project \n
    :returns: DialogFlow-Agent"""
    return MANAGER.get_agent()


def train_agent():
    """issues a training command to the linked agent to train him on the uploaded intents"""
    return MANAGER.train_agent()


def list_entity_types():
    """:returns: a list of all DialogFlow-EntityTypes for the registered Agent/Project"""
    return MANAGER.list_entity_types()


def list_intents():
    """:returns: a list of DialogFlow-Intents"""
    return MANAGER.list_intents()


def update_entity_types(entity_type_name="", entities_to_add=None, entities_to_remove=None):
    """
    Updates an entity type by adding or removing entities
    :param entity_type_name: str (display name of the entity type)
    :param entities_to_add: [str] (the entities we want to add to the type, if any)
    :param entities_to_remove: [str] (the entities we want to remove from the type, if any)
    :returns: ?
    """
    return MANAGER.update_entity_types(
        entity_type_name=entity_type_name,
        entities_to_add=entities_to_add,
        entities_to_remove=entities_to_remove)


def batch_upload_entity_types(entity_types_to_create=None):
    """
    loads the entity types to the dialogflow agent
    :param entity_types_to_create: [Entity_Type] (List of Entity Types to upload)
    :returns: ?
    """
    return MANAGER.batch_upload_entity_types(entity_types_to_create=entity_types_to_create)


def batch_upload_intents(intents_to_create=None):
    """
    loads the intents to the dialogflow agent
    :param intents_to_create: [Intent] (List of Intents to upload)
    :returns: ?
    """
    return MANAGER.batch_upload_intents(intents_to_create=intents_to_create)


def respond(input_="", session_id=None, contexts=None):
    """
    gets the DialogFlow-Response to a given input_
    :param input_: str (the input_)
    :param session_id: str (a session id under which the conversation is held, will be linked to users later)
    :param contexts: [str] (a list of the Context Names we want our Session to have at the moment)
    :returns: str
    """
    return MANAGER.respond(input_=input_, session_id=session_id, contexts=contexts)
