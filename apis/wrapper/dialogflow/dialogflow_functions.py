"""
This file holds all dialogflow functions that are independent of the login information
"""

import google.cloud.dialogflow_v2 as dialogflow_vx

from utils.errors import *


def create_entity_type(display_name="", entity_entries=None):
    """creates a new Entity-Type in the registered Agent
    :param display_name: str (the name to represent this entity [keep it ascii])
    :param entity_entries: [str] List of entries for this entity type
    :returns: an instance of the created entityType """
    if not display_name or not entity_entries:
        raise MissingArgument(f"Not all necessary parameters filled sufficiently: \n{display_name=}\n"
                              f"{entity_entries=}")
    # sanitizing display name
    un_allowed_chars = "?:;,.(){}!\\/[]'\"#*+=-\t\näÄöÖüÜßéèêÉÈÊáàâÁÀÂ°óòôÓÒÔíìîÍÌÎúùûÚÙÛß"
    for char in un_allowed_chars:
        display_name = display_name.replace(char, "")
    # The entity_type to be added:
    new_entity_type = dialogflow_vx.EntityType()
    new_entity_type.display_name = display_name
    # we don't need to map synonyms to the corresponding entries, so a list is sufficient
    new_entity_type.kind = "KIND_LIST"
    # remove duplicates
    entity_entries = list(set(entity_entries))
    # build the entites as a list
    entity_list = []
    for entity in entity_entries:
        entity_list.append(
            dialogflow_vx.EntityType.Entity(value=entity, synonyms=[entity])
        )
    # add the entities to our entity_type
    new_entity_type.entities = entity_list

    return new_entity_type


def build_phrases(training_phrases="", entity_type_required=None):
    """builds training phrases from a stringlist and annotates them if entity_types are given
            :param training_phrases: [str] (a list of training phrases on which the bot should learn)
            :param entity_type_required: [Dialog Entity Type] (the entity types we want to annotate)
            :returns: a DialogFlow training phrases object"""
    if not training_phrases:
        raise MissingArgument(f"Not all necessary parameters filled sufficiently: \n{training_phrases=}")
    annotated_training_phrases = []
    if not entity_type_required:
        # phrases without annotation
        for sentence in training_phrases:
            part = dialogflow_vx.Intent.TrainingPhrase.Part()
            part.text = sentence
            annotated_training_phrases.append(dialogflow_vx.Intent.TrainingPhrase(parts=[part]))

    else:
        for sentence in training_phrases:
            # phrases with annotation
            # split sentence
            parts = sentence.split(" ")
            i = 0  # index
            parts_copy = parts.copy()
            un_allowed_chars = "!,?."
            for elem in parts_copy:
                for char in un_allowed_chars:
                    if len(elem) > 1:
                        if elem[-1] == char:
                            parts[i] = parts[i].replace(char, "")
                            parts.insert(i + 1, char)
            i += 1
            # annotate sentences
            final_phrase = []
            for part in parts:
                this_part = dialogflow_vx.types.Intent.TrainingPhrase.Part()
                this_part.text = part
                for entity in entity_type_required.entities:
                    if part == entity.value:
                        this_part.entity_type = "@" + entity_type_required.display_name
                        this_part.user_defined = True
                        this_part.alias = entity_type_required.display_name
                # add to final composition
                final_phrase.append(this_part)
                # add a whitespace after:
                white_part = dialogflow_vx.types.Intent.TrainingPhrase.Part()
                white_part.text = " "
                final_phrase.append(white_part)

            # the part annotation should get highlighted wrong. How do I know?
            # Google uses it like this in their examples
            current_phrase = dialogflow_vx.Intent.TrainingPhrase()
            current_phrase.parts = final_phrase
            annotated_training_phrases.append(current_phrase)
    return annotated_training_phrases


def create_intent(training_phrases=None, response="", display_name="", required_entity_type=None,
                  prompts_for_entity_type=None):
    """Creates a DialogFlow-Intent object with annotated training phrases
    :param training_phrases: [str] (a list of training phrases on which the bot should learn)
    :param response: str (the identified intent e.g. skip, a specific feature or a decision in the dialogue tree)
    :param display_name: str (display name of the intent [special char's allowed])
    :param required_entity_type: dialogflow entity type (the entity types we want to retrieve from the user, if any)
    :param prompts_for_entity_type: [str] (the follow up questions that must be asked
                                    to retrieve the required entity_type, if any)
    :returns: ?"""
    if not training_phrases or not response or not display_name:
        raise MissingArgument(f"Not all necessary parameters filled sufficiently: \n{training_phrases=}\n"
                              f"{response=}\n{display_name=}")
    if not not required_entity_type and not prompts_for_entity_type:
        raise ParameterRequired("if entities are required, proper prompts are required as well")

    # sanitizing display name
    un_allowed_chars = "?:;,.(){}!\\/[]'\"#*+=-\t\näÄöÖüÜßéèêÉÈÊáàâÁÀÂ°óòôÓÒÔíìîÍÌÎúùûÚÙÛß"
    for char in un_allowed_chars:
        display_name = display_name.replace(char, "")

    # building the intent
    new_intent = dialogflow_vx.Intent()
    new_intent.display_name = display_name
    message = dialogflow_vx.Intent.Message()
    text = dialogflow_vx.Intent.Message.Text()
    text.text = [response]
    message.text = text
    new_intent.messages = [message]
    if not not required_entity_type:

        new_parameter = dialogflow_vx.Intent.Parameter()
        new_parameter.name = required_entity_type.name
        new_parameter.display_name = required_entity_type.display_name
        new_parameter.mandatory = True
        new_parameter.prompts = prompts_for_entity_type
        new_parameter.entity_type_display_name = "@" + required_entity_type.display_name
        new_intent.parameters = [new_parameter]

        new_intent.training_phrases = build_phrases(training_phrases, required_entity_type)
    else:
        new_intent.training_phrases = build_phrases(training_phrases)

    return new_intent
