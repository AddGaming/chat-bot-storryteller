## what is this project?

We try to apply NLU, LNP, and other language systems to identify the way the user wants to experience a certain story.
While we start out from a simple graph based model we want to procedural generate as much content as possible in the end.

## accessing from remote

We will try to host a version of this on a private cloud where you can access it for free and play with it. 
If there is no link here this means the platform got taken down or it didnt make it into the cloud yet.

## Hosting your self

Just download the files from the main branch and execute the main.py in the base directory.
If you get an error message, try installing all libraries mentioned in the requirements.txt.

Afterwards the application should be accessible in your local network at port 5000.

## Fefe code
O++S+(+)!IC+EMV+PS++D

## Documentation
Check the gitlab wiki. We documented everything